import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPoll } from 'app/shared/model/poll.model';
import { PollService } from './poll.service';

@Component({
  selector: 'jhi-poll-delete-dialog',
  templateUrl: './poll-delete-dialog.component.html'
})
export class PollDeleteDialogComponent {
  poll: IPoll;

  constructor(protected pollService: PollService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.pollService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'pollListModification',
        content: 'Deleted an poll'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-poll-delete-popup',
  template: ''
})
export class PollDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ poll }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PollDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.poll = poll;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/poll', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/poll', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
