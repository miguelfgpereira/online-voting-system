import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OvsSharedModule } from 'app/shared/shared.module';
import { OptionComponent } from './option.component';
import { OptionDetailComponent } from './option-detail.component';
import { OptionUpdateComponent } from './option-update.component';
import { OptionDeletePopupComponent, OptionDeleteDialogComponent } from './option-delete-dialog.component';
import { optionRoute, optionPopupRoute } from './option.route';

const ENTITY_STATES = [...optionRoute, ...optionPopupRoute];

@NgModule({
  imports: [OvsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [OptionComponent, OptionDetailComponent, OptionUpdateComponent, OptionDeleteDialogComponent, OptionDeletePopupComponent],
  entryComponents: [OptionDeleteDialogComponent]
})
export class OvsOptionModule {}
