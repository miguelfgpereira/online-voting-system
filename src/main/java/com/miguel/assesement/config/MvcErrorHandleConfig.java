//package com.miguel.assesement.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerExceptionResolver;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
//
//import java.util.List;
//
//@Configuration
//public class MvcErrorHandleConfig extends WebMvcConfigurationSupport {
//    @Autowired
//    @Qualifier("OvsErrorHandle")
//    private HandlerExceptionResolver handlerExceptionResolver;
//
//    @Override
//    protected void extendHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
//        exceptionResolvers.add(0, handlerExceptionResolver);
//    }
//}
