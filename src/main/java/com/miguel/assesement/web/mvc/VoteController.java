package com.miguel.assesement.web.mvc;

import com.miguel.assesement.service.dto.OptionDTO;
import com.miguel.assesement.service.dto.VoteDTO;
import com.miguel.assesement.web.rest.OptionResource;
import com.miguel.assesement.web.rest.PollResource;
import com.miguel.assesement.web.rest.VoteResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

import java.net.URISyntaxException;

@Controller
@RequestMapping("/mvc/vote")
public class VoteController {
    private final Logger log = LoggerFactory.getLogger(VoteController.class);
    private PollResource pollResource;
    private VoteResource voteResource;
    private OptionResource optionResource;
    private MvcUtils mvcUtils;

    public VoteController(PollResource pollResource, VoteResource voteResource, OptionResource optionResource, MvcUtils mvcUtils) {
        this.pollResource = pollResource;
        this.voteResource = voteResource;
        this.optionResource = optionResource;
        this.mvcUtils = mvcUtils;
    }


    @PostMapping("{pollId}")
    public RedirectView vote(@PathVariable Long pollId, VoteDTO voteDTO) throws URISyntaxException {
        log.debug("MVC request to Vote : PollId = {} , VoteDTO = {}", pollId, voteDTO);
        OptionDTO optionDTO = optionResource.getOption(voteDTO.getOptionId()).getBody();
            if(optionDTO == null || !optionDTO.getPollId().equals(pollId)) return new RedirectView("/mvc/poll/"+pollId);
        voteDTO.setPollId(pollId);
        voteDTO.setUserId(mvcUtils.getCurrentUser().getId());
        voteResource.createVote(voteDTO);
        return new RedirectView("/mvc/poll/"+pollId);
    }
}
