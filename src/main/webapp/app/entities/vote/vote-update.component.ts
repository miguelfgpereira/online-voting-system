import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IVote, Vote } from 'app/shared/model/vote.model';
import { VoteService } from './vote.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IOption } from 'app/shared/model/option.model';
import { OptionService } from 'app/entities/option/option.service';
import { IPoll } from 'app/shared/model/poll.model';
import { PollService } from 'app/entities/poll/poll.service';

@Component({
  selector: 'jhi-vote-update',
  templateUrl: './vote-update.component.html'
})
export class VoteUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  options: IOption[];

  polls: IPoll[];

  editForm = this.fb.group({
    id: [],
    userId: [],
    optionId: [],
    pollId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected voteService: VoteService,
    protected userService: UserService,
    protected optionService: OptionService,
    protected pollService: PollService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ vote }) => {
      this.updateForm(vote);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.optionService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IOption[]>) => mayBeOk.ok),
        map((response: HttpResponse<IOption[]>) => response.body)
      )
      .subscribe((res: IOption[]) => (this.options = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.pollService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IPoll[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPoll[]>) => response.body)
      )
      .subscribe((res: IPoll[]) => (this.polls = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(vote: IVote) {
    this.editForm.patchValue({
      id: vote.id,
      userId: vote.userId,
      optionId: vote.optionId,
      pollId: vote.pollId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const vote = this.createFromForm();
    if (vote.id !== undefined) {
      this.subscribeToSaveResponse(this.voteService.update(vote));
    } else {
      this.subscribeToSaveResponse(this.voteService.create(vote));
    }
  }

  private createFromForm(): IVote {
    return {
      ...new Vote(),
      id: this.editForm.get(['id']).value,
      userId: this.editForm.get(['userId']).value,
      optionId: this.editForm.get(['optionId']).value,
      pollId: this.editForm.get(['pollId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVote>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackOptionById(index: number, item: IOption) {
    return item.id;
  }

  trackPollById(index: number, item: IPoll) {
    return item.id;
  }
}
