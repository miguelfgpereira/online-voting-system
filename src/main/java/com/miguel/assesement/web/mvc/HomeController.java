package com.miguel.assesement.web.mvc;


import com.miguel.assesement.security.SecurityUtils;
import com.miguel.assesement.service.dto.CategoryDTO;
import com.miguel.assesement.service.dto.PollCriteria;
import com.miguel.assesement.service.dto.PollDTO;
import com.miguel.assesement.web.rest.CategoryResource;
import com.miguel.assesement.web.rest.PollResource;
import com.miguel.assesement.web.rest.StatisticsResource;
import com.miguel.assesement.web.rest.VoteResource;
import io.github.jhipster.service.filter.LocalDateFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/mvc")
public class HomeController {

    private final Logger log = LoggerFactory.getLogger(HomeController.class);
    private PollResource pollResource;
    private MvcUtils mvcUtils;
    private VoteResource voteResource;
    private StatisticsResource statisticsResource;
    private CategoryResource categoryResource;


    public HomeController(CategoryResource categoryResource,StatisticsResource statisticsResource,VoteResource voteResource, MvcUtils mvcUtils, PollResource pollResource) {
        this.voteResource = voteResource;
        this.mvcUtils = mvcUtils;
        this.pollResource = pollResource;
        this.statisticsResource = statisticsResource;
        this.categoryResource = categoryResource;
    }


    @GetMapping("")
    public ModelAndView home(Pageable pageable, @RequestParam(defaultValue = "", name="name") String name) {
        log.debug("MVC Request to show Home Page : {} , {}", pageable,name);
        return activePolls(pageable, name);
    }

    @GetMapping("/ActivePolls")
    public ModelAndView activePolls(Pageable pageable, @RequestParam(defaultValue = "", name="name") String name) {
        log.debug("MVC Request to show Active Polls Page : {} , {}", pageable,name);
        return genericPollList(pageable,PollCriteriaFactory.getActivePollCriteria(name),"Active Polls");
    }

    @GetMapping("/PastPolls")
    public ModelAndView pastPolls(Pageable pageable, @RequestParam(defaultValue = "", name="name") String name) {
        log.debug("MVC Request to show Past Polls Page : {} , {}", pageable,name);
        return genericPollList(pageable,PollCriteriaFactory.getPastPollCriteria(name),"Past Polls");
    }

    @GetMapping("/FuturePolls")
    public ModelAndView futurePolls(Pageable pageable, @RequestParam(defaultValue = "", name="name") String name) {
        log.debug("MVC Request to show Future Page : {} , {}", pageable,name);
        return genericPollList(pageable,PollCriteriaFactory.getFuturePollCriteria(name),"Future Polls");
    }

    @GetMapping("/AnsweredPolls")
    public ModelAndView answeredPolls(Pageable pageable, @RequestParam(defaultValue = "", name="name") String name) {
        log.debug("MVC Request to show Answered Polls Page : {} , {}, {}", pageable,name, SecurityUtils.getCurrentUserLogin());
        if(name==null || name.trim().isEmpty())
            return genericPollList(pageable,pollResource.getAnsweredPolls(pageable),"Answered Polls");

        return genericPollList(pageable,pollResource.getAnsweredPollsFilterByName(pageable,name),"Answered Polls");
    }

    @GetMapping("/UnansweredPolls")
    public ModelAndView unansweredPolls(Pageable pageable, @RequestParam(defaultValue = "", name="name") String name) {
        log.debug("MVC Request to show Unanswered Polls Page : {} , {}, {}", pageable,name, SecurityUtils.getCurrentUserLogin());
        if(name==null || name.trim().isEmpty())
            return genericPollList(pageable,pollResource.getUnanswered(pageable), "Unanswered Polls");

        return genericPollList(pageable,pollResource.getUnansweredFilterByName(pageable,name), "Unanswered Polls");
    }

    @GetMapping("/Statistics")
    public ModelAndView statistics() {
        log.debug("MVC Request to show Statistics Page");
        ModelAndView result = mvcUtils.getModelAndView(null);
        result = MvcUtils.insertMapinModel(result, statisticsResource.applicationStatistics().getBody(), null);
        List<CategoryDTO> categories = statisticsResource.getPopularCategories(5).getBody();
        if(categories!=null) {
            result.addObject("categoriesPercentage", getCategoriesPercentage(categories));
            result.addObject("categoriesList", categories);
        }
        result.setViewName("statistics");
        return result;
    }

    private Map<Long, Long> getCategoriesPercentage(List<CategoryDTO> categories) {
        Map<Long,Long> result = new HashMap<>();
        for(CategoryDTO categoryDTO : categories) {
            Long number = categoryResource.getCategoryPollCount(categoryDTO.getId()).getBody();
            result.putIfAbsent(categoryDTO.getId(),number);
        }
        return result;
    }

    private ModelAndView genericPollList(Pageable pageable,ResponseEntity<List<PollDTO>> request,String text) {
        List<PollDTO> polls = request.getBody();
        ModelAndView result = mvcUtils.getModelAndView(request);
        result.addObject("polls",polls);
        result.addObject("pageName",text);
        result.setViewName("home");
        return result;
    }

    private ModelAndView genericPollList(Pageable pageable,PollCriteria pollCriteria,String text) {
        ResponseEntity<List<PollDTO>> request = pollResource.getAllPolls(pollCriteria,pageable);
        List<PollDTO> polls = request.getBody();
        ModelAndView result = mvcUtils.getModelAndView(request);
        result.addObject("polls",polls);
        result.addObject("pageName",text);
        result.setViewName("home");
        return result;
    }

}
