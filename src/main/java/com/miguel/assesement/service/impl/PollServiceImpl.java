package com.miguel.assesement.service.impl;

import com.miguel.assesement.security.SecurityUtils;
import com.miguel.assesement.service.PollService;
import com.miguel.assesement.domain.Poll;
import com.miguel.assesement.repository.PollRepository;
import com.miguel.assesement.service.UserService;
import com.miguel.assesement.service.dto.PollDTO;
import com.miguel.assesement.service.mapper.PollMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Poll}.
 */
@Service
@Transactional
public class PollServiceImpl implements PollService {

    private final Logger log = LoggerFactory.getLogger(PollServiceImpl.class);

    private final PollRepository pollRepository;

    private final PollMapper pollMapper;

    private final UserService userService;

    public PollServiceImpl(UserService userService,PollRepository pollRepository, PollMapper pollMapper) {
        this.pollRepository = pollRepository;
        this.pollMapper = pollMapper;
        this.userService=userService;
    }

    /**
     * Save a poll.
     *
     * @param pollDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PollDTO save(PollDTO pollDTO) {
        log.debug("Request to save Poll : {}", pollDTO);
        pollDTO.setUserId(userService.getUserWithAuthoritiesByLogin(SecurityUtils.getCurrentUserLogin().get()).get().getId());
        Poll poll = pollMapper.toEntity(pollDTO);
        poll = pollRepository.save(poll);
        return pollMapper.toDto(poll);
    }

    /**
     * Get all the polls.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PollDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Polls");
        return pollRepository.findAll(pageable)
            .map(pollMapper::toDto);
    }


    /**
     * Get one poll by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PollDTO> findOne(Long id) {
        log.debug("Request to get Poll : {}", id);
        return pollRepository.findById(id)
            .map(pollMapper::toDto);
    }

    /**
     * Delete the poll by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Poll : {}", id);
        pollRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PollDTO> getAnsweredPolls(Pageable pageable) {
        log.debug("Request to get answered polls");
        return pollRepository.getAnsweredPolls(pageable).map(pollMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PollDTO> getAnsweredPollsFilterByName(Pageable pageable, String name) {
        log.debug("Request to get answered polls filtered by name : {}", name);
        return pollRepository.getAnsweredPollsFilterByName(pageable,name).map(pollMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PollDTO> getUnansweredPolls(Pageable pageable) {
        log.debug("Request to get Unanswered polls");
        return pollRepository.getUnansweredPolls(pageable).map(pollMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PollDTO> getUnansweredPollsFilterByName(Pageable pageable, String name) {
        log.debug("Request to get Unanswered polls filtered by name : {}", name);
        return pollRepository.getUnansweredPollsFilterByName(pageable,name).map(pollMapper::toDto);
    }

    @Override
    public Long getTotalPollsCount() {
        log.debug("Request to get Total Polls Count");
        Long result = pollRepository.countAllPolls();
        return (result!=null ? result : 0);
    }
}
