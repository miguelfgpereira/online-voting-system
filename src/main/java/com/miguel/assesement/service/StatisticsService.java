package com.miguel.assesement.service;

import com.miguel.assesement.service.dto.CategoryDTO;

import java.util.List;
import java.util.Map;

public interface StatisticsService {

    Map<String, String> getApplicationStatistics();

    List<CategoryDTO> getTopNPopularCategories(int number);
}
