import { Moment } from 'moment';
import { IOption } from 'app/shared/model/option.model';
import { IVote } from 'app/shared/model/vote.model';

export interface IPoll {
  id?: number;
  name?: string;
  description?: string;
  initialDate?: Moment;
  endDate?: Moment;
  options?: IOption[];
  votes?: IVote[];
  userLogin?: string;
  userId?: number;
  categoryName?: string;
  categoryId?: number;
}

export class Poll implements IPoll {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public initialDate?: Moment,
    public endDate?: Moment,
    public options?: IOption[],
    public votes?: IVote[],
    public userLogin?: string,
    public userId?: number,
    public categoryName?: string,
    public categoryId?: number
  ) {}
}
