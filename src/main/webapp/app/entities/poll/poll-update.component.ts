import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IPoll, Poll } from 'app/shared/model/poll.model';
import { PollService } from './poll.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';

@Component({
  selector: 'jhi-poll-update',
  templateUrl: './poll-update.component.html'
})
export class PollUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  categories: ICategory[];
  initialDateDp: any;
  endDateDp: any;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(50)]],
    description: [null, [Validators.minLength(5)]],
    initialDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
    userId: [],
    categoryId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected pollService: PollService,
    protected userService: UserService,
    protected categoryService: CategoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ poll }) => {
      this.updateForm(poll);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.categoryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICategory[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICategory[]>) => response.body)
      )
      .subscribe((res: ICategory[]) => (this.categories = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(poll: IPoll) {
    this.editForm.patchValue({
      id: poll.id,
      name: poll.name,
      description: poll.description,
      initialDate: poll.initialDate,
      endDate: poll.endDate,
      userId: poll.userId,
      categoryId: poll.categoryId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const poll = this.createFromForm();
    if (poll.id !== undefined) {
      this.subscribeToSaveResponse(this.pollService.update(poll));
    } else {
      this.subscribeToSaveResponse(this.pollService.create(poll));
    }
  }

  private createFromForm(): IPoll {
    return {
      ...new Poll(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      description: this.editForm.get(['description']).value,
      initialDate: this.editForm.get(['initialDate']).value,
      endDate: this.editForm.get(['endDate']).value,
      userId: this.editForm.get(['userId']).value,
      categoryId: this.editForm.get(['categoryId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPoll>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackCategoryById(index: number, item: ICategory) {
    return item.id;
  }
}
