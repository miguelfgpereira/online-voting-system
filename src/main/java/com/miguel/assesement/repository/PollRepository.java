package com.miguel.assesement.repository;
import com.miguel.assesement.domain.Poll;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.DoubleStream;

/**
 * Spring Data  repository for the Poll entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PollRepository extends JpaRepository<Poll, Long>, JpaSpecificationExecutor<Poll> {

    @Query("select poll from Poll poll where poll.user.login = ?#{principal.username}")
    List<Poll> findByUserIsCurrentUser();

    @Query("select poll from Poll poll join poll.votes vote where vote.user.login = ?#{principal.username}")
    Page<Poll> getAnsweredPolls(Pageable pageable);

    @Query("select poll from Poll poll join poll.votes vote where vote.user.login = ?#{principal.username} and poll.name like '%' || :name || '%'")
    Page<Poll> getAnsweredPollsFilterByName(Pageable pageable, @Param("name") String name);

    @Query("select poll from Poll poll where ?#{principal.username} not in (select vote.user.login from Vote vote where vote.poll.id = poll.id) and poll.initialDate <= current_date() and poll.endDate >= current_date()")
    Page<Poll> getUnansweredPolls(Pageable pageable);

    @Query("select poll from Poll poll where ?#{principal.username} not in (select vote.user.login from Vote vote where vote.poll.id = poll.id) and poll.initialDate <= current_date() and poll.endDate >= current_date() and poll.name like '%' || :name || '%'")
    Page<Poll> getUnansweredPollsFilterByName(Pageable pageable, @Param("name") String name);

    @Query("select count(*) from Poll poll")
    Long countAllPolls();
}
