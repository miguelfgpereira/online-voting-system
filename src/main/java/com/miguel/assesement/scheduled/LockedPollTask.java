package com.miguel.assesement.scheduled;

import com.miguel.assesement.domain.User;
import com.miguel.assesement.service.*;
import com.miguel.assesement.service.dto.OptionDTO;
import com.miguel.assesement.service.dto.PollCriteria;
import com.miguel.assesement.service.dto.PollDTO;
import com.miguel.assesement.service.dto.UserDTO;
import io.github.jhipster.service.filter.LocalDateFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class LockedPollTask {
    private static final Logger log = LoggerFactory.getLogger(LockedPollTask.class);
    private PollQueryService pollQueryService;
    private VoteService voteService;
    private OptionService optionService;
    private UserService userService;
    private MailService mailService;

    public LockedPollTask(MailService mailService, UserService userService, PollQueryService pollQueryService, VoteService voteService, OptionService optionService) {
        this.pollQueryService = pollQueryService;
        this.voteService = voteService;
        this.optionService = optionService;
        this.userService = userService;
        this.mailService = mailService;
    }

    @Scheduled(cron = "0 10 * * * *")
//    @Scheduled(fixedRate = 5000)
    public void checkLockedPolls() {
        log.debug("The time is now : {}", LocalDate.now());
        List<PollDTO> polls = pollQueryService.findByCriteria(getLockedCriteria());
        for(PollDTO poll : polls) {
            OptionDTO option = optionService.getWinningOption(poll.getId());
            long percentage=0;
            if(option!=null)
               percentage = voteService.getOptionPercentage(option.getId());
            List<UserDTO> users = getUsersToEmail(poll);
            for(UserDTO user : users) {
                mailService.sendLockedPollEmail(user,option,percentage,poll);
            }
        }
    }

    private List<UserDTO> getUsersToEmail(PollDTO poll) {
        List<UserDTO> result = voteService.getUsersThatVoted(poll.getId());
        Optional<User> domainUser = userService.getUserWithAuthorities(poll.getUserId());
        domainUser.ifPresent(user -> result.add(new UserDTO(user)));
        return result;
    }

    private PollCriteria getLockedCriteria() {
        PollCriteria pollCriteria = new PollCriteria();
        LocalDateFilter endDateFilter = new LocalDateFilter();
        endDateFilter.setEquals(LocalDate.now().minusDays(1));
        pollCriteria.setEndDate(endDateFilter);
        return pollCriteria;
    }

}
