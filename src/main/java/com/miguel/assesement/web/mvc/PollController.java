package com.miguel.assesement.web.mvc;

import com.miguel.assesement.service.dto.OptionDTO;
import com.miguel.assesement.service.dto.PollDTO;
import com.miguel.assesement.web.rest.CategoryResource;
import com.miguel.assesement.web.rest.OptionResource;
import com.miguel.assesement.web.rest.PollResource;
import com.miguel.assesement.web.rest.VoteResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
@RequestMapping("/mvc/poll")
public class PollController {
    private final Logger log = LoggerFactory.getLogger(PollController.class);
    private MvcUtils mvcUtils;
    private PollResource pollResource;
    private CategoryResource categoryResource;
    private OptionResource optionResource;
    private VoteResource voteResource;

    public PollController(VoteResource voteResource, OptionResource optionResource, PollResource pollResource, MvcUtils mvcUtils, CategoryResource categoryResource) {
        this.optionResource = optionResource;
        this.pollResource = pollResource;
        this.mvcUtils = mvcUtils;
        this.categoryResource = categoryResource;
        this.voteResource = voteResource;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        log.debug("MVC Request to show create poll page");
        ModelAndView result = mvcUtils.getModelAndView(null);
        result.setViewName("poll/create");
        result.addObject("categories", categoryResource.getAllForListing().getBody());
        return result;
    }

    @PostMapping("")
    public RedirectView save(@RequestBody MultiValueMap<String, String> formData, RedirectAttributes redirectAttributes) throws URISyntaxException {
        log.debug("MVC Request to poll : {}", formData);
        Map<String,String> singleFormData = formData.toSingleValueMap();
        PollDTO pollDTO = formatFormMappingToPollDTO(singleFormData);
        if(pollDTO==null) {
            redirectAttributes.addFlashAttribute("error","Failed to create poll");
            return new RedirectView("/mvc/poll/create");
        }
        List<OptionDTO> optionsDTO = formatFormMappingToListOptionsDTO(formData);
        if(optionsDTO==null || optionsDTO.size()==0) {
            redirectAttributes.addFlashAttribute("error","Failed to create poll due to options");
            return new RedirectView("/mvc/poll/create");
        }

        pollDTO = pollResource.createPoll(pollDTO).getBody();
        if(pollDTO==null) {
            redirectAttributes.addFlashAttribute("error","Failed to create pol");
            return new RedirectView("/mvc/poll/create");
        }
        Long pollId = pollDTO.getId();
        optionsDTO.forEach((e) -> e.setPollId(pollId));


        optionResource.createListOfOptions(optionsDTO);
        redirectAttributes.addFlashAttribute("success","Created poll with success");
        return new RedirectView("/mvc/poll/"+pollDTO.getId());
    }

//    @GetMapping("")
//    public ModelAndView index(Pageable pageable) {
//        ModelAndView result = mvcUtils.getModelAndView(null);
//        return result;
//    }

    @GetMapping("/{id}")
    public ModelAndView view(@PathVariable Long id, Pageable pageable) {
        log.debug("MVC Request to show poll page : {} , {}", id, pageable);
        ModelAndView result = mvcUtils.getModelAndView(null);
        PollDTO pollDTO = pollResource.getPoll(id).getBody();
        if(pollDTO==null) return new ModelAndView("redirect:/mvc/poll");
        boolean voted=false;
        if(mvcUtils.isUserLoggedIn())
            voted = voteResource.currentUserAlreadyVoted(id).getBody();
        List<OptionDTO> optionsDTO = optionResource.getPollOptions(pollDTO.getId());
        boolean canVote = canUserVote(pollDTO);
        boolean locked = isPollLocked(pollDTO);
        if(voted) {
            result.addObject("votedOptionId", voteResource.getVotedOption(id).getBody());
        }
        if(voted || locked || !mvcUtils.isUserLoggedIn()) {
            result.addObject("totalVotes", voteResource.getTotalVotes(id).getBody());
            result.addObject("optionPercentage", getPercentageForOptions(optionsDTO));
        }
        result.addObject("canVote",canVote);
        result.addObject("locked",locked);
        result.addObject("voted", voted);
        result.addObject("options", optionsDTO);

        result.addObject("poll",pollDTO);
        result.setViewName("poll/view");
        return result;
    }

    private boolean isPollLocked(PollDTO pollDTO) {
        LocalDate today = LocalDate.now();
        return today.isAfter(pollDTO.getEndDate());
    }

    private boolean canUserVote(PollDTO pollDTO) {
        LocalDate today = LocalDate.now();
        return today.isAfter(pollDTO.getInitialDate().minusDays(1)) && today.isBefore(pollDTO.getEndDate().plusDays(1));
    }

//    @GetMapping("/{id}/edit")
//    public ModelAndView edit(@PathVariable Long id) {
//        ModelAndView result = mvcUtils.getModelAndView(null);
//        return result;
//    }
//
//    @PostMapping("/{id}")
//    public RedirectView update(@PathVariable Long id, PollDTO pollDTO) {
//        return new RedirectView("/mvc/poll/"+id);
//    }
//
//    @PostMapping("/{id}/delete")
//    public RedirectView delete(@PathVariable Long id) {
//        return new RedirectView("/mvc/poll");
//    }

    private PollDTO formatFormMappingToPollDTO(Map<String,String> formData) {
        PollDTO result = new PollDTO();
        long categoryId = Long.parseLong(formData.getOrDefault("category_id","-1"));
        if(categoryId==-1) return null;
        result.setCategoryId(categoryId);
        result.setDescription(formData.getOrDefault("description", "no description"));
        String name = formData.getOrDefault("name",null);
        if(name==null) return null;
        result.setName(name);
        String date = formData.getOrDefault("date", null);
        if(date==null) return null;
        String[] arrDate = date.split("-");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/uuuu");

        result.setInitialDate(LocalDate.parse(arrDate[0].trim(),format));
        result.setEndDate(LocalDate.parse(arrDate[1].trim(),format));


        return result;
    }

    private List<OptionDTO> formatFormMappingToListOptionsDTO(MultiValueMap<String, String> formData) {
        List<String> options = formData.getOrDefault("option[]", null);
        List<OptionDTO> optionsDTO = new ArrayList<>();
        if(options==null) return null;

        for(String s : options) {
            if(s!=null && !s.trim().isEmpty()) {
                OptionDTO optionDTO = new OptionDTO();
                optionDTO.setName(s);
                optionsDTO.add(optionDTO);
            }
        }

        return optionsDTO;
    }

    private Map<Long, Long> getPercentageForOptions(List<OptionDTO> optionsDTO) {
        Map<Long,Long> result = new HashMap<>();
        for(OptionDTO optionDTO : optionsDTO) {
            result.put(optionDTO.getId(),voteResource.getOptionPercentage(optionDTO.getId()).getBody());
        }
        return result;
    }
}
