package com.miguel.assesement.service.impl;

import com.miguel.assesement.service.CategoryService;
import com.miguel.assesement.domain.Category;
import com.miguel.assesement.repository.CategoryRepository;
import com.miguel.assesement.service.dto.CategoryDTO;
import com.miguel.assesement.service.mapper.CategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Category}.
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);

    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    /**
     * Save a category.
     *
     * @param categoryDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CategoryDTO save(CategoryDTO categoryDTO) {
        log.debug("Request to save Category : {}", categoryDTO);
        Category category = categoryMapper.toEntity(categoryDTO);
        category = categoryRepository.save(category);
        return categoryMapper.toDto(category);
    }

    /**
     * Get all the categories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Categories");
        return categoryRepository.findAll(pageable)
            .map(categoryMapper::toDto);
    }


    /**
     * Get one category by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CategoryDTO> findOne(Long id) {
        log.debug("Request to get Category : {}", id);
        return categoryRepository.findById(id)
            .map(categoryMapper::toDto);
    }

    /**
     * Delete the category by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Category : {}", id);
        categoryRepository.deleteById(id);
    }

    @Override
    public Map<String, String> getStatistics(Long id) {
        log.debug("Request to get Category statistics : {}", id);
        Map<String, String> result = new HashMap<String, String>();
        result.put("polls", this.getCategoryPollCount(id).toString());
        result.put("options", categoryRepository.getCategoryOptionsCount(id).toString());
        result.put("votes", categoryRepository.getCategoryVotesCount(id).toString());
        return result;
    }

    @Override
    public List<CategoryDTO> getCategoriesForListing() {
        log.debug("Request to get all Categories for Listing");
        return categoryRepository.getCategoriesForListing();
    }

    @Override
    public Long getTotalCategoriesCount() {
        log.debug("Request to get total Categories Count");
        Long result = categoryRepository.CountAllCategories();
        return (result!=null ? result : 0);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CategoryDTO> getPopularCategories(int number) {
        log.debug("Request to get popular categories");

        return categoryRepository.findAllByOrderByPopularity(PageRequest.of(0,number))
                                                         .map(categoryMapper::toDto)
                                                         .getContent();
    }

    @Override
    public Long getCategoryPollCount(Long id) {
        log.debug("Request to get CategoryPollCount");
        Long result = categoryRepository.getCategoryPollCount(id);
        return (result!=null ? result : 0);
    }
}
