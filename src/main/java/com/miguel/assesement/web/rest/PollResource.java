package com.miguel.assesement.web.rest;

import com.miguel.assesement.service.PollService;
import com.miguel.assesement.web.rest.errors.BadRequestAlertException;
import com.miguel.assesement.service.dto.PollDTO;
import com.miguel.assesement.service.dto.PollCriteria;
import com.miguel.assesement.service.PollQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.miguel.assesement.domain.Poll}.
 */
@RestController
@RequestMapping("/api")
public class PollResource {

    private final Logger log = LoggerFactory.getLogger(PollResource.class);

    private static final String ENTITY_NAME = "poll";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PollService pollService;

    private final PollQueryService pollQueryService;

    public PollResource(PollService pollService, PollQueryService pollQueryService) {
        this.pollService = pollService;
        this.pollQueryService = pollQueryService;
    }

    /**
     * {@code POST  /polls} : Create a new poll.
     *
     * @param pollDTO the pollDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pollDTO, or with status {@code 400 (Bad Request)} if the poll has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/polls")
    public ResponseEntity<PollDTO> createPoll(@Valid @RequestBody PollDTO pollDTO) throws URISyntaxException {
        log.debug("REST request to save Poll : {}", pollDTO);
        if (pollDTO.getId() != null) {
            throw new BadRequestAlertException("A new poll cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PollDTO result = pollService.save(pollDTO);
        return ResponseEntity.created(new URI("/api/polls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /polls} : Updates an existing poll.
     *
     * @param pollDTO the pollDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pollDTO,
     * or with status {@code 400 (Bad Request)} if the pollDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pollDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/polls")
    public ResponseEntity<PollDTO> updatePoll(@Valid @RequestBody PollDTO pollDTO) throws URISyntaxException {
        log.debug("REST request to update Poll : {}", pollDTO);
        if (pollDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PollDTO result = pollService.save(pollDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pollDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /polls} : get all the polls.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of polls in body.
     */
    @GetMapping("/polls")
    public ResponseEntity<List<PollDTO>> getAllPolls(PollCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Polls by criteria: {}", criteria);
        Page<PollDTO> page = pollQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /polls/count} : count all the polls.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/polls/count")
    public ResponseEntity<Long> countPolls(PollCriteria criteria) {
        log.debug("REST request to count Polls by criteria: {}", criteria);
        return ResponseEntity.ok().body(pollQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /polls/:id} : get the "id" poll.
     *
     * @param id the id of the pollDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pollDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/polls/{id}")
    public ResponseEntity<PollDTO> getPoll(@PathVariable Long id) {
        log.debug("REST request to get Poll : {}", id);
        Optional<PollDTO> pollDTO = pollService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pollDTO);
    }

    /**
     * {@code DELETE  /polls/:id} : delete the "id" poll.
     *
     * @param id the id of the pollDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/polls/{id}")
    public ResponseEntity<Void> deletePoll(@PathVariable Long id) {
        log.debug("REST request to delete Poll : {}", id);
        pollService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/polls/AnsweredPolls")
    public ResponseEntity<List<PollDTO>>  getAnsweredPolls(Pageable pageable) {
        log.debug("REST request to get Answered Polls");
        Page<PollDTO> page = pollService.getAnsweredPolls(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    public ResponseEntity<List<PollDTO>> getAnsweredPollsFilterByName(Pageable pageable, String name) {
        log.debug("REST request to get Answered Polls filtered by name : {}", name);
        Page<PollDTO> page = pollService.getAnsweredPollsFilterByName(pageable,name);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    public ResponseEntity<List<PollDTO>> getUnanswered(Pageable pageable) {
        log.debug("REST request to get Unanswered Polls");
        Page<PollDTO> page = pollService.getUnansweredPolls(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    public ResponseEntity<List<PollDTO>> getUnansweredFilterByName(Pageable pageable, String name) {
        log.debug("REST request to get Unanswered Polls filtered by name : {}", name);
        Page<PollDTO> page = pollService.getUnansweredPollsFilterByName(pageable,name);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
