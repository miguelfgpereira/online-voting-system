package com.miguel.assesement.repository;
import com.miguel.assesement.domain.Category;
import com.miguel.assesement.service.dto.CategoryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query("Select count(*) from Poll poll where poll.category.id = :categoryId")
    Long getCategoryPollCount(@Param("categoryId") Long id);

    @Query("Select count(*) from Option option where option.poll.category.id = :categoryId")
    Long getCategoryOptionsCount(@Param("categoryId") Long id);

    @Query("Select count(*) from Vote vote where vote.poll.category.id = :categoryId")
    Long getCategoryVotesCount(@Param("categoryId") Long id);

    @Query("Select new com.miguel.assesement.service.dto.CategoryDTO(category.id,category.name) from Category category")
    List<CategoryDTO> getCategoriesForListing();

    @Query("select count(*) from Category category")
    Long CountAllCategories();

    @Query("select category from Category category left join category.polls poll group by category order by count(poll) desc")
    Page<Category> findAllByOrderByPopularity(Pageable pageable);
}
