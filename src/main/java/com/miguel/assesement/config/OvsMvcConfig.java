package com.miguel.assesement.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
public class OvsMvcConfig extends WebMvcConfigurerAdapter {

    @Autowired
    @Qualifier("OvsErrorHandle")
    private HandlerExceptionResolver handlerExceptionResolver;

    @Override
    public void extendHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
        exceptionResolvers.add(0, handlerExceptionResolver);
    }

    //https://memorynotfound.com/adding-static-resources-css-javascript-images-thymeleaf/
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(
            "/plugins/**",
                          "/dist/**")
            .addResourceLocations(
                "classpath:/plugins/",
                "classpath:/dist/");

    }

}
