package com.miguel.assesement.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.miguel.assesement.domain.Option} entity.
 */
public class OptionDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;


    private Long pollId;

    private String pollName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPollId() {
        return pollId;
    }

    public void setPollId(Long pollId) {
        this.pollId = pollId;
    }

    public String getPollName() {
        return pollName;
    }

    public void setPollName(String pollName) {
        this.pollName = pollName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OptionDTO optionDTO = (OptionDTO) o;
        if (optionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), optionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OptionDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", poll=" + getPollId() +
            ", poll='" + getPollName() + "'" +
            "}";
    }
}
