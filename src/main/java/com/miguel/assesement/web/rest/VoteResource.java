package com.miguel.assesement.web.rest;

import com.miguel.assesement.service.VoteService;
import com.miguel.assesement.web.rest.errors.BadRequestAlertException;
import com.miguel.assesement.service.dto.VoteDTO;

import com.sun.mail.iap.Response;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.miguel.assesement.domain.Vote}.
 */
@RestController
@RequestMapping("/api")
public class VoteResource {

    private final Logger log = LoggerFactory.getLogger(VoteResource.class);

    private static final String ENTITY_NAME = "vote";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VoteService voteService;

    public VoteResource(VoteService voteService) {
        this.voteService = voteService;
    }

    /**
     * {@code POST  /votes} : Create a new vote.
     *
     * @param voteDTO the voteDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new voteDTO, or with status {@code 400 (Bad Request)} if the vote has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/votes")
    public ResponseEntity<VoteDTO> createVote(@RequestBody VoteDTO voteDTO) throws URISyntaxException {
        log.debug("REST request to save Vote : {}", voteDTO);
        if (voteDTO.getId() != null) {
            throw new BadRequestAlertException("A new vote cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VoteDTO result = voteService.save(voteDTO);
        if(result!=null)
            return ResponseEntity.created(new URI("/api/votes/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
        else
            return ResponseEntity.unprocessableEntity().build();
    }

    /**
     * {@code PUT  /votes} : Updates an existing vote.
     *
     * @param voteDTO the voteDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated voteDTO,
     * or with status {@code 400 (Bad Request)} if the voteDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the voteDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/votes")
    public ResponseEntity<VoteDTO> updateVote(@RequestBody VoteDTO voteDTO) throws URISyntaxException {
        log.debug("REST request to update Vote : {}", voteDTO);
        if (voteDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VoteDTO result = voteService.save(voteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, voteDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /votes} : get all the votes.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of votes in body.
     */
    @GetMapping("/votes")
    public List<VoteDTO> getAllVotes() {
        log.debug("REST request to get all Votes");
        return voteService.findAll();
    }

    /**
     * {@code GET  /votes/:id} : get the "id" vote.
     *
     * @param id the id of the voteDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the voteDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/votes/{id}")
    public ResponseEntity<VoteDTO> getVote(@PathVariable Long id) {
        log.debug("REST request to get Vote : {}", id);
        Optional<VoteDTO> voteDTO = voteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(voteDTO);
    }

    /**
     * {@code DELETE  /votes/:id} : delete the "id" vote.
     *
     * @param id the id of the voteDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/votes/{id}")
    public ResponseEntity<Void> deleteVote(@PathVariable Long id) {
        log.debug("REST request to delete Vote : {}", id);
        voteService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/votes/currentUserAlreadyVoted/{pollId}")
    public ResponseEntity<Boolean> currentUserAlreadyVoted(@PathVariable Long pollId) {
        log.debug("REST request do check if current user already voted : {}", pollId);
        Boolean result = voteService.didCurrentUserAlreadyVote(pollId);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/votes/getVotedOption/{pollId}")
    public ResponseEntity<Long> getVotedOption(@PathVariable Long pollId) {
        log.debug("REST request to get current user voted option : {}", pollId);
        Long result = voteService.getVotedOption(pollId);
        if(result == -1)
            return ResponseEntity.badRequest().build();
        else
            return ResponseEntity.ok().body(result);
    }

    @GetMapping("/votes/getOptionPercentage/{optionId}")
    public ResponseEntity<Long> getOptionPercentage(@PathVariable Long optionId) {
        log.debug("REST request to get option percentage : {}", optionId);
        return ResponseEntity.ok().body(voteService.getOptionPercentage(optionId));
    }

    @GetMapping("/votes/getPollTotalVotes/{pollId}")
    public ResponseEntity<Long> getTotalVotes(@PathVariable Long pollId) {
        log.debug("REST request to get Poll total votes : {}",pollId);
        return ResponseEntity.ok().body(voteService.getPollTotalVotes(pollId));
    }
}
