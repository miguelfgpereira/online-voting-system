package com.miguel.assesement.service;

import com.miguel.assesement.service.dto.OptionDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.miguel.assesement.domain.Option}.
 */
public interface OptionService {

    /**
     * Save a option.
     *
     * @param optionDTO the entity to save.
     * @return the persisted entity.
     */
    OptionDTO save(OptionDTO optionDTO);

    /**
     * Get all the options.
     *
     * @return the list of entities.
     */
    List<OptionDTO> findAll();


    /**
     * Get the "id" option.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OptionDTO> findOne(Long id);

    /**
     * Delete the "id" option.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<OptionDTO> getPollOptions(Long pollId);

    Long getTotalOptionsCount();

    OptionDTO getWinningOption(Long id);
}
