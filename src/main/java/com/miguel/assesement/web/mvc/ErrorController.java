package com.miguel.assesement.web.mvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ErrorController {

    private final Logger log = LoggerFactory.getLogger(ErrorController.class);

    @GetMapping("errors")
    public ModelAndView error(){
        log.debug("MVC request to show an error page");
        ModelAndView result = new ModelAndView("debug");
        return result;
    }
}
