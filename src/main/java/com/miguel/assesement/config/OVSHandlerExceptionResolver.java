package com.miguel.assesement.config;

import com.miguel.assesement.web.mvc.MvcUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Locale;

@Component("OvsErrorHandle")
public class OVSHandlerExceptionResolver implements HandlerExceptionResolver {

    private final Logger log = LoggerFactory.getLogger(OVSHandlerExceptionResolver.class);

    @Autowired
    private ViewResolver viewResolver;
    @Autowired
    private MvcUtils mvcUtils;

    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object handler, Exception ex) {
        if(! httpServletRequest.getRequestURI().startsWith("/mvc")) return null;
        ModelAndView mav = mvcUtils.getModelAndView(null);
        mav.setViewName("error");
        populateModelAndViewForException(mav,ex);
        renderView(httpServletRequest,httpServletResponse,mav);
        return mav;
    }

    private void populateModelAndViewForException(ModelAndView mav, Exception ex) {
        int errorCode = 500;
        String errorMessage = "Server Internal Error";
        //TODO add exception types
        if(ex instanceof AccessDeniedException) {
           errorCode = 403;
           errorMessage = ex.getMessage();
        } else if(ex instanceof InsufficientAuthenticationException) {
            errorCode = 401;
            errorMessage = ex.getMessage();
        }

        mav.addObject("errorCode", errorCode);
        mav.addObject("errorMessage", errorMessage);
        log.debug("Exception caught : {}", ex.getMessage());
    }

    private void renderView(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, ModelAndView mav) {
        View view = mav.getView();
        try {
            view = viewResolver.resolveViewName(mav.getViewName(), Locale.getDefault());
        } catch (Exception ex) {
            log.debug("Exception caught while resolving view name: {}", Arrays.toString(ex.getStackTrace()));
        }
        if(view == null) return;
        try {
            view.render(mav.getModel(),httpServletRequest,httpServletResponse);
        } catch (Exception ex) {
            log.debug("Exception caught while rendering view: {}", Arrays.toString(ex.getStackTrace()));
        }
    }
}


