package com.miguel.assesement.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.miguel.assesement.domain.Poll} entity. This class is used
 * in {@link com.miguel.assesement.web.rest.PollResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /polls?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PollCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter description;

    private LocalDateFilter initialDate;

    private LocalDateFilter endDate;

    private LongFilter optionsId;

    private LongFilter votesId;

    private LongFilter userId;

    private LongFilter categoryId;

    public PollCriteria(){
    }

    public PollCriteria(PollCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.initialDate = other.initialDate == null ? null : other.initialDate.copy();
        this.endDate = other.endDate == null ? null : other.endDate.copy();
        this.optionsId = other.optionsId == null ? null : other.optionsId.copy();
        this.votesId = other.votesId == null ? null : other.votesId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.categoryId = other.categoryId == null ? null : other.categoryId.copy();
    }

    @Override
    public PollCriteria copy() {
        return new PollCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LocalDateFilter getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(LocalDateFilter initialDate) {
        this.initialDate = initialDate;
    }

    public LocalDateFilter getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateFilter endDate) {
        this.endDate = endDate;
    }

    public LongFilter getOptionsId() {
        return optionsId;
    }

    public void setOptionsId(LongFilter optionsId) {
        this.optionsId = optionsId;
    }

    public LongFilter getVotesId() {
        return votesId;
    }

    public void setVotesId(LongFilter votesId) {
        this.votesId = votesId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(LongFilter categoryId) {
        this.categoryId = categoryId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PollCriteria that = (PollCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(description, that.description) &&
            Objects.equals(initialDate, that.initialDate) &&
            Objects.equals(endDate, that.endDate) &&
            Objects.equals(optionsId, that.optionsId) &&
            Objects.equals(votesId, that.votesId) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(categoryId, that.categoryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        description,
        initialDate,
        endDate,
        optionsId,
        votesId,
        userId,
        categoryId
        );
    }

    @Override
    public String toString() {
        return "PollCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (initialDate != null ? "initialDate=" + initialDate + ", " : "") +
                (endDate != null ? "endDate=" + endDate + ", " : "") +
                (optionsId != null ? "optionsId=" + optionsId + ", " : "") +
                (votesId != null ? "votesId=" + votesId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (categoryId != null ? "categoryId=" + categoryId + ", " : "") +
            "}";
    }

}
