package com.miguel.assesement.web.mvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class ResourceController {
    private final Logger log = LoggerFactory.getLogger(ResourceController.class);
    private final Path path;

    public ResourceController() {
        path= Paths.get(new File("src\\main\\resources\\dist\\img").getPath());
    }


    @GetMapping("userProfile/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serverFile(@PathVariable String filename){
        log.debug("MVC Request to get userProfile Picture : {}", filename);
        try {
            Resource file = loadAsResource(filename);
            if(file == null)
                return ResponseEntity.badRequest().build();

            return ResponseEntity
                .ok()
                .body(file);
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    public Resource loadAsResource(String filename) throws MalformedURLException {
        Path file = path.resolve(filename);
        Resource resource = new UrlResource(file.toUri());
        if(resource.exists() || resource.isReadable())
            return resource;
        return null;
    }
}
