package com.miguel.assesement.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.miguel.assesement.domain.Poll;
import com.miguel.assesement.domain.*; // for static metamodels
import com.miguel.assesement.repository.PollRepository;
import com.miguel.assesement.service.dto.PollCriteria;
import com.miguel.assesement.service.dto.PollDTO;
import com.miguel.assesement.service.mapper.PollMapper;

/**
 * Service for executing complex queries for {@link Poll} entities in the database.
 * The main input is a {@link PollCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PollDTO} or a {@link Page} of {@link PollDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PollQueryService extends QueryService<Poll> {

    private final Logger log = LoggerFactory.getLogger(PollQueryService.class);

    private final PollRepository pollRepository;

    private final PollMapper pollMapper;

    public PollQueryService(PollRepository pollRepository, PollMapper pollMapper) {
        this.pollRepository = pollRepository;
        this.pollMapper = pollMapper;
    }

    /**
     * Return a {@link List} of {@link PollDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PollDTO> findByCriteria(PollCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Poll> specification = createSpecification(criteria);
        return pollMapper.toDto(pollRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PollDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PollDTO> findByCriteria(PollCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Poll> specification = createSpecification(criteria);
        return pollRepository.findAll(specification, page)
            .map(pollMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PollCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Poll> specification = createSpecification(criteria);
        return pollRepository.count(specification);
    }

    /**
     * Function to convert {@link PollCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Poll> createSpecification(PollCriteria criteria) {
        Specification<Poll> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Poll_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Poll_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Poll_.description));
            }
            if (criteria.getInitialDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInitialDate(), Poll_.initialDate));
            }
            if (criteria.getEndDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndDate(), Poll_.endDate));
            }
            if (criteria.getOptionsId() != null) {
                specification = specification.and(buildSpecification(criteria.getOptionsId(),
                    root -> root.join(Poll_.options, JoinType.LEFT).get(Option_.id)));
            }
            if (criteria.getVotesId() != null) {
                specification = specification.and(buildSpecification(criteria.getVotesId(),
                    root -> root.join(Poll_.votes, JoinType.LEFT).get(Vote_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Poll_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getCategoryId() != null) {
                specification = specification.and(buildSpecification(criteria.getCategoryId(),
                    root -> root.join(Poll_.category, JoinType.LEFT).get(Category_.id)));
            }
        }
        return specification;
    }
}
