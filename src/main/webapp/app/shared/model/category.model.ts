import { IPoll } from 'app/shared/model/poll.model';

export interface ICategory {
  id?: number;
  name?: string;
  polls?: IPoll[];
}

export class Category implements ICategory {
  constructor(public id?: number, public name?: string, public polls?: IPoll[]) {}
}
