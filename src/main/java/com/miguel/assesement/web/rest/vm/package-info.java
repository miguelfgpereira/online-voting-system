/**
 * View Models used by Spring MVC REST controllers.
 */
package com.miguel.assesement.web.rest.vm;
