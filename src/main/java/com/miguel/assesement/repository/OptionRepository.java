package com.miguel.assesement.repository;
import com.miguel.assesement.domain.Option;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;


/**
 * Spring Data  repository for the Option entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OptionRepository extends JpaRepository<Option, Long> {

    List<Option> findAllByPollId(Long pollId);

    @Query("select count(*) from Option option")
    Long CountAllOptions();
}
