import { IVote } from 'app/shared/model/vote.model';

export interface IOption {
  id?: number;
  name?: string;
  votes?: IVote[];
  pollName?: string;
  pollId?: number;
}

export class Option implements IOption {
  constructor(public id?: number, public name?: string, public votes?: IVote[], public pollName?: string, public pollId?: number) {}
}
