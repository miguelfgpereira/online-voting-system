package com.miguel.assesement.web.mvc;

import com.miguel.assesement.service.CategoryService;
import com.miguel.assesement.service.dto.CategoryDTO;
import com.miguel.assesement.service.dto.PollCriteria;
import com.miguel.assesement.service.dto.PollDTO;
import com.miguel.assesement.web.rest.CategoryResource;
import com.miguel.assesement.web.rest.PollResource;
import io.github.jhipster.service.filter.LongFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;


import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/mvc/category")
public class CategoryController {

    private final Logger log = LoggerFactory.getLogger(CategoryController.class);

    private MvcUtils mvcUtils;
    private CategoryResource categoryResource;
    private PollResource pollResource;
    private CategoryService categoryService;

    public CategoryController(PollResource pollResource, MvcUtils mvcUtils, CategoryResource categoryResource, CategoryService categoryService) {
        this.pollResource = pollResource;
        this.mvcUtils = mvcUtils;
        this.categoryResource = categoryResource;
        this.categoryService = categoryService;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        log.debug("MVC Request to show Create Category Page");
        ModelAndView result = mvcUtils.getModelAndView(null);
        result.setViewName("category/create");
        return result;
    }

    @PostMapping("")
    public RedirectView save(CategoryDTO categoryDTO, RedirectAttributes redirectAttributes) throws URISyntaxException {
        log.debug("MVC Request to create Category : {}", categoryDTO);
        try {
            categoryDTO = categoryResource.createCategory(categoryDTO).getBody();
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", "Failed to create category");
            return new RedirectView("/mvc/category/create");
        }
        if(categoryDTO!=null) {
            redirectAttributes.addFlashAttribute("success", "Created category with success");
            return new RedirectView("/mvc/category/" + categoryDTO.getId());
        }
        redirectAttributes.addFlashAttribute("error", "Failed to create category");
        return new RedirectView("/mvc/category/create");
    }

    @GetMapping("")
    public ModelAndView index(Pageable pageable) {
        log.debug("MVC Request to show Index Category Page : {}", pageable);
        ResponseEntity<List<CategoryDTO>> request = categoryResource.getAllCategories(pageable);
        ModelAndView result = mvcUtils.getModelAndView(request);
        result.addObject("categories", request.getBody());
        result.setViewName("category/index");
        return result;
    }

    @GetMapping("/{id}")
    public ModelAndView view(@PathVariable Long id, Pageable pageable) {
        log.debug("MVC Request to show Category Page : {} , {}",id, pageable);
        ResponseEntity<CategoryDTO> request = categoryResource.getCategory(id);
        if(!request.hasBody()) return new ModelAndView("redirect:/mvc/category");
        PollCriteria pollCriteria = new PollCriteria();
        LongFilter catFilter = new LongFilter();
        catFilter.setEquals(id);
        pollCriteria.setCategoryId(catFilter);
        ResponseEntity<List<PollDTO>> pollRequest = pollResource.getAllPolls(pollCriteria, pageable);

        ModelAndView result = mvcUtils.getModelAndView(pollRequest);
        result.addObject("polls", pollRequest.getBody());
        result.addObject("category",request.getBody());
        MvcUtils.insertMapinModel(result, categoryResource.getCategoryStatistics(id).getBody(),"stats");
        result.setViewName("category/view");

        return result;
    }

    @GetMapping("/{id}/edit")
    public ModelAndView edit(@PathVariable Long id) {
        log.debug("MVC Request to show Edit Category Page : {}", id);
        ResponseEntity<CategoryDTO> request = categoryResource.getCategory(id);
        if(!request.hasBody()) return new ModelAndView("redirect:/mvc/category");

        ModelAndView result = mvcUtils.getModelAndView(null);
        result.addObject("category",request.getBody());
        result.setViewName("category/edit");

        return result;
    }

    @PostMapping("/{id}")
    public RedirectView update(@PathVariable Long id, CategoryDTO categoryDTO,RedirectAttributes redirectAttributes) throws URISyntaxException {
        log.debug("MVC Request to update Category : {} , {}", id, categoryDTO);
        categoryDTO.setId(id);
        categoryResource.updateCategory(categoryDTO);
        redirectAttributes.addFlashAttribute("success", "Updated category with success");
        return new RedirectView("/mvc/category/"+id);
    }

    @PostMapping("/{id}/delete")
    public RedirectView delete(@PathVariable Long id,RedirectAttributes redirectAttributes) {
        log.debug("MVC Request to delete Category Page : {}", id);
        categoryResource.deleteCategory(id);
        redirectAttributes.addFlashAttribute("success", "Deleted category with success");
        return new RedirectView("/mvc/category");
    }

    @PostMapping("ajax/create")
    @ResponseBody
    public String createAjax(CategoryDTO categoryDTO) {
        log.debug("MVC Request to Create Category using AJAX : {}", categoryDTO);
        try {
            categoryDTO = categoryResource.createCategory(categoryDTO).getBody();
            if(categoryDTO == null) return "{ \"type\": \"error\" }";
            return String.format("{ \"type\": \"success\", \"id\": %d}",categoryDTO.getId());
        } catch (Exception e) {
            return "{ \"type\": \"error\" }";
        }
    }
}
