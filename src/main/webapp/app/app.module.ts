import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { OvsSharedModule } from 'app/shared/shared.module';
import { OvsCoreModule } from 'app/core/core.module';
import { OvsAppRoutingModule } from './app-routing.module';
import { OvsHomeModule } from './home/home.module';
import { OvsEntityModule } from './entities/entity.module';
import { OvsLogoutModule } from './custom/logout/logout.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    OvsSharedModule,
    OvsCoreModule,
    OvsHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    OvsEntityModule,
    OvsLogoutModule,
    OvsAppRoutingModule
  ],
  declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [JhiMainComponent]
})
export class OvsAppModule {}
