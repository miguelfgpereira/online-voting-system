import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from 'app/core/login/login.service';

@Component({
  selector: 'jhi-logout',
  templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit {
  constructor(private loginService: LoginService, private router: Router) {}

  ngOnInit() {
    this.logout();
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['']);
  }
}
