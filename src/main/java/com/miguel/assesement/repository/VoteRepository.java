package com.miguel.assesement.repository;
import com.miguel.assesement.domain.User;
import com.miguel.assesement.domain.Vote;
import com.miguel.assesement.service.dto.UserDTO;
import com.miguel.assesement.service.dto.VoteDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Vote entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {

    @Query("select vote from Vote vote where vote.user.login = ?#{principal.username}")
    List<Vote> findByUserIsCurrentUser();

    @Query("select count(*)>0 from Vote vote where vote.user.login = ?#{principal.username} and vote.poll.id = :pollId")
    Boolean didCurrentUserAlreadyVote(@Param("pollId") Long pollId);

    @Query("select count(*)>0 from Vote vote where vote.user.id = :userId and vote.poll.id = :pollId")
    Boolean DoesVoteAlreadyExist(@Param("pollId") Long pollId, @Param("userId") Long userId);

    @Query("select vote.option.id from Vote vote where vote.user.login = ?#{principal.username} and vote.poll.id = :pollId")
    Long getVotedOption(@Param("pollId") Long pollId);

    //@Query("select (Count(*)/(select Count(*) from Vote v where v.poll.id = vote.option.poll.id)) from Vote vote where vote.option.id=:optionId")
//    @Query("select (Count(vote.id) / (select count(v.id) from Vote v where v.poll.id=vote.option.poll.id))*100 from Vote vote where vote.option.id = :optionId")
//    Long getOptionPercentage(@Param("optionId") Long optionId);

    @Query("select count(*) from Vote vote where vote.option.id = :optionId")
    Long getOptionTotalVotes(@Param("optionId") Long optionId);

    @Query("select Count(*) from Vote vote where vote.poll.id = :pollId")
    Long getPollTotalVotes(@Param("pollId") Long pollId);

    @Query("select count(*) from Vote vote")
    Long CountAllVotes();

    @Query("select vote.user from Vote vote where vote.poll.id = :pollId")
    List<User> getUsersThatVotedOnPoll(@Param("pollId") Long pollId);

    @Query("select count(*) from Vote vote where vote.poll.id in (select v.poll.id from Vote v where v.option.id = :optionId)")
    Long getPollTotalVotesByOptionID(@Param("optionId") Long optionId);
}
