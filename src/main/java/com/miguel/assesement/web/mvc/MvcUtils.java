package com.miguel.assesement.web.mvc;

import com.miguel.assesement.security.SecurityUtils;
import com.miguel.assesement.service.dto.UserDTO;
import com.miguel.assesement.web.rest.UserResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.spring5.expression.Mvc;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

@Component
public final class MvcUtils {
    private UserResource userResource;

    public MvcUtils(UserResource userResource) {this.userResource = userResource;}

    private Map<String,Integer> getPaginationMapfromRequest(ResponseEntity<?> response) {
        Map<String,Integer> result = new HashMap<>();
        String Link = response.getHeaders().getFirst("Link");
        if(Link == null || Link.trim().isEmpty())
            return null;

        String[] firstSplit = Link.split(",");
        for(String s : firstSplit) {
            String[] secondSplit = s.split(";");
            if(secondSplit.length==2) {
                String key = secondSplit[1];
                String value = secondSplit[0];
                key = key.substring(6,key.length()-1);
                value = value.substring(1,value.length()-1);
                value = value.split("page=")[1].split("&size")[0];
                result.put(key, Integer.parseInt(value));
            }
        }
        if(result.containsKey("prev"))
            result.putIfAbsent("current", result.get("prev")+1);
        if(result.containsKey("next"))
            result.putIfAbsent("current", result.get("next")-1);
        if(result.containsKey("last"))
            result.putIfAbsent("current", result.get("last"));


        return result;
    }

    public ModelAndView getModelAndView(@Nullable ResponseEntity<?> request) {
        ModelAndView result = new ModelAndView();
        if(request != null)
            insertMapinModel(result,getPaginationMapfromRequest(request),"pagination");
        UserDTO user = getCurrentUser();
        if(user==null || user.getLogin().toLowerCase().equals("anonymoususer")) {
            result.addObject("authenticated",false);
        } else {
            result.addObject("authenticated",true);
            result.addObject("isAdmin",(isUserAdmin(user)));
            result.addObject("user", user);
        }

       return result;
    }

    private boolean isUserAdmin(UserDTO user) {
        if(user!=null && user.getAuthorities()!=null)
            for(String s : user.getAuthorities())
                if(s.equals("ROLE_ADMIN"))
                    return true;

        return false;
    }

    public UserDTO getCurrentUser() {
        UserDTO result;
        String login = SecurityUtils.getCurrentUserLogin().orElse(null);
        if(login==null)
            return null;

        return userResource.getUser(login).getBody();
    }

    public Boolean isUserLoggedIn() {
        return SecurityUtils.getCurrentUserLogin().isPresent() && !SecurityUtils.getCurrentUserLogin().get().toLowerCase().equals("anonymoususer");
    }

    public static ModelAndView insertMapinModel(ModelAndView result, Map<String, ?> map, String prefix) {
        if(map==null) return result;
        Iterator it = map.entrySet().iterator();
        if(prefix==null) prefix="";
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            String key = pair.getKey().toString();
            if(!prefix.trim().isEmpty())
                key = key.substring(0, 1).toUpperCase() + key.substring(1);
            result.addObject(prefix+key,pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }
        return result;
    }

}
