export interface IVote {
  id?: number;
  userLogin?: string;
  userId?: number;
  optionName?: string;
  optionId?: number;
  pollName?: string;
  pollId?: number;
}

export class Vote implements IVote {
  constructor(
    public id?: number,
    public userLogin?: string,
    public userId?: number,
    public optionName?: string,
    public optionId?: number,
    public pollName?: string,
    public pollId?: number
  ) {}
}
