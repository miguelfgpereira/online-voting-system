import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OvsSharedModule } from 'app/shared/shared.module';
import { logoutRoute } from './logout.route';
import { LogoutComponent } from './logout.component';

@NgModule({
  imports: [RouterModule.forRoot(logoutRoute)],
  declarations: [LogoutComponent]
})
export class OvsLogoutModule {}
