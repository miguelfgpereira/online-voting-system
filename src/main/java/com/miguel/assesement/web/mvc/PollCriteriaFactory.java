package com.miguel.assesement.web.mvc;

import com.miguel.assesement.service.dto.PollCriteria;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.StringFilter;

import javax.annotation.Nullable;
import java.time.LocalDate;

public final class PollCriteriaFactory {
    public static PollCriteria getFuturePollCriteria(@Nullable String name) {
        PollCriteria result = new PollCriteria();
        LocalDateFilter initialDateFilter = new LocalDateFilter();
        initialDateFilter.setGreaterThan(LocalDate.now());
        result.setInitialDate(initialDateFilter);

        if(name!=null && !name.trim().isEmpty()) {
            result = setNameFilter(result,name);
        }

        return result;
    }

    public static PollCriteria getPastPollCriteria(@Nullable String name) {
        PollCriteria result = new PollCriteria();
        LocalDateFilter endDateFilter = new LocalDateFilter();
        endDateFilter.setLessThan(LocalDate.now());
        result.setEndDate(endDateFilter);

        if(name!=null && !name.trim().isEmpty()) {
            setNameFilter(result,name);
        }

        return result;
    }

    public static PollCriteria getActivePollCriteria(@Nullable String name) {
        PollCriteria result = new PollCriteria();
        LocalDateFilter initialDateFilter = new LocalDateFilter();
        initialDateFilter.setLessThanOrEqual(LocalDate.now());
        result.setInitialDate(initialDateFilter);
        LocalDateFilter endDateFilter = new LocalDateFilter();
        endDateFilter.setGreaterThanOrEqual(LocalDate.now());
        result.setEndDate(endDateFilter);

        if(name!=null && !name.trim().isEmpty()) {
            result = setNameFilter(result,name);
        }

        return result;
    }

    private static PollCriteria setNameFilter(PollCriteria result, String name) {
        StringFilter nameFilter = new StringFilter();
        nameFilter.setContains(name);
        result.setName(nameFilter);
        return result;
    }
}
