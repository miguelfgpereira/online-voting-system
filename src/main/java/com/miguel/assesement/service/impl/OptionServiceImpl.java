package com.miguel.assesement.service.impl;

import com.miguel.assesement.service.OptionService;
import com.miguel.assesement.domain.Option;
import com.miguel.assesement.repository.OptionRepository;
import com.miguel.assesement.service.VoteService;
import com.miguel.assesement.service.dto.OptionDTO;
import com.miguel.assesement.service.mapper.OptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Option}.
 */
@Service
@Transactional
public class OptionServiceImpl implements OptionService {

    private final Logger log = LoggerFactory.getLogger(OptionServiceImpl.class);

    private final OptionRepository optionRepository;

    private final OptionMapper optionMapper;

    private final VoteService voteService;

    public OptionServiceImpl(VoteService voteService, OptionRepository optionRepository, OptionMapper optionMapper) {
        this.optionRepository = optionRepository;
        this.optionMapper = optionMapper;
        this.voteService = voteService;
    }

    /**
     * Save a option.
     *
     * @param optionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OptionDTO save(OptionDTO optionDTO) {
        log.debug("Request to save Option : {}", optionDTO);
        Option option = optionMapper.toEntity(optionDTO);
        option = optionRepository.save(option);
        return optionMapper.toDto(option);
    }

    /**
     * Get all the options.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<OptionDTO> findAll() {
        log.debug("Request to get all Options");
        return optionRepository.findAll().stream()
            .map(optionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one option by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OptionDTO> findOne(Long id) {
        log.debug("Request to get Option : {}", id);
        return optionRepository.findById(id)
            .map(optionMapper::toDto);
    }

    /**
     * Delete the option by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Option : {}", id);
        optionRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OptionDTO> getPollOptions(Long pollId) {
        log.debug("Request to get all Poll Options");
        return optionRepository.findAllByPollId(pollId).stream()
            .map(optionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Long getTotalOptionsCount() {
        log.debug("Request to get total Options Count");
        Long result = optionRepository.CountAllOptions();
        return  (result!=null ? result : 0);
    }

    @Override
    public OptionDTO getWinningOption(Long pollId) {
        log.debug("Request to get winning option for poll : {}", pollId);
        List<OptionDTO> options = this.getPollOptions(pollId);
        int max = 0;
        OptionDTO result = null;
        for(OptionDTO option : options) {
            long percentage = this.voteService.getOptionPercentage(option.getId());
            if(percentage>max) {
                max= (int) percentage;
                result = option;
            }
        }

        return result;
    }
}
