package com.miguel.assesement.repository;

import com.miguel.assesement.domain.PersistentAuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data JPA repository for the {@link PersistentAuditEvent} entity.
 */
public interface PersistenceAuditEventRepository extends JpaRepository<PersistentAuditEvent, Long> {


    List<PersistentAuditEvent> findByPrincipal(String principal);

    List<PersistentAuditEvent> findByPrincipalAndAuditEventDateAfterAndAuditEventType(String principal, Instant after, String type);

    Page<PersistentAuditEvent> findAllByAuditEventDateBetween(Instant fromDate, Instant toDate, Pageable pageable);

    List<PersistentAuditEvent> findByAuditEventDateBefore(Instant before);

    @Query("select count(*) from PersistentAuditEvent ev where ev.auditEventType = 'AUTHENTICATION_SUCCESS' and " +
           "year(ev.auditEventDate) = year(current_date()) and month(ev.auditEventDate) = month(current_date()) and " +
           "day(ev.auditEventDate) = day(current_date())")
    Long countSuccessfullLoginsToday();

    @Query("select count(distinct ev.principal) from PersistentAuditEvent ev where ev.auditEventType = 'AUTHENTICATION_SUCCESS' and " +
        "year(ev.auditEventDate) = year(current_date()) and month(ev.auditEventDate) = month(current_date()) and " +
        "day(ev.auditEventDate) = day(current_date())")
    Long countUniqueSuccessfullLoginsToday();
}
