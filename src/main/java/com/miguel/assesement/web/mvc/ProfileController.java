package com.miguel.assesement.web.mvc;

import com.miguel.assesement.security.SecurityUtils;
import com.miguel.assesement.service.UserService;
import com.miguel.assesement.service.dto.UserDTO;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

@Controller
@RequestMapping("/mvc")
public class ProfileController {
    private final Logger log = LoggerFactory.getLogger(ProfileController.class);
    private MvcUtils mvcUtils;
    private UserService userService;

    public ProfileController(UserService userService, MvcUtils mvcUtils) {
        this.userService = userService;
        this.mvcUtils = mvcUtils;
    }

    @GetMapping("/profile")
    public ModelAndView userProfile() {
        log.debug("MVC Request to show User Profile : {}", SecurityUtils.getCurrentUserLogin());
        ModelAndView result = mvcUtils.getModelAndView(null);
        result.setViewName("user/profile");
        return result;
    }

    @PostMapping("/changepicture")
    public ResponseEntity<Object> pictureUpload(@RequestParam("file") MultipartFile file) {
        log.debug("MVC Request to change User Picture AJAX : {}", SecurityUtils.getCurrentUserLogin());
        UserDTO user = mvcUtils.getCurrentUser();
        String imgPath = "src\\main\\resources\\dist\\img";
        try {
            String extension = FilenameUtils.getExtension(file.getOriginalFilename());
            String filename = user.getId()+"."+extension;
            BufferedOutputStream outputStream = new BufferedOutputStream(
                new FileOutputStream(
                    new File(imgPath, filename)));
            outputStream.write(file.getBytes());
            outputStream.flush();
            outputStream.close();
            user.setImageUrl(filename);
            userService.updateUser(user);

        return new ResponseEntity<>("200",HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("409",HttpStatus.CONFLICT);
        }

        //return new ResponseEntity<>("teste", HttpStatus.OK);
    }


}
