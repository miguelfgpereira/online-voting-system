package com.miguel.assesement.service.mapper;

import com.miguel.assesement.domain.*;
import com.miguel.assesement.service.dto.VoteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Vote} and its DTO {@link VoteDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, OptionMapper.class, PollMapper.class})
public interface VoteMapper extends EntityMapper<VoteDTO, Vote> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "option.id", target = "optionId")
    @Mapping(source = "option.name", target = "optionName")
    @Mapping(source = "poll.id", target = "pollId")
    @Mapping(source = "poll.name", target = "pollName")
    VoteDTO toDto(Vote vote);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "optionId", target = "option")
    @Mapping(source = "pollId", target = "poll")
    Vote toEntity(VoteDTO voteDTO);

    default Vote fromId(Long id) {
        if (id == null) {
            return null;
        }
        Vote vote = new Vote();
        vote.setId(id);
        return vote;
    }
}
