package com.miguel.assesement.service.impl;

import com.miguel.assesement.service.VoteService;
import com.miguel.assesement.domain.Vote;
import com.miguel.assesement.repository.VoteRepository;
import com.miguel.assesement.service.dto.UserDTO;
import com.miguel.assesement.service.dto.VoteDTO;
import com.miguel.assesement.service.mapper.VoteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Vote}.
 */
@Service
@Transactional
public class VoteServiceImpl implements VoteService {

    private final Logger log = LoggerFactory.getLogger(VoteServiceImpl.class);

    private final VoteRepository voteRepository;

    private final VoteMapper voteMapper;

    public VoteServiceImpl(VoteRepository voteRepository, VoteMapper voteMapper) {
        this.voteRepository = voteRepository;
        this.voteMapper = voteMapper;
    }

    /**
     * Save a vote.
     *
     * @param voteDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public VoteDTO save(VoteDTO voteDTO) {
        log.debug("Request to save Vote : {}", voteDTO);
        Vote vote = voteMapper.toEntity(voteDTO);
        if(!voteRepository.DoesVoteAlreadyExist(voteDTO.getPollId(),voteDTO.getUserId())) {
            vote = voteRepository.save(vote);
            return voteMapper.toDto(vote);
        }
        return null;
    }

    /**
     * Get all the votes.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<VoteDTO> findAll() {
        log.debug("Request to get all Votes");
        return voteRepository.findAll().stream()
            .map(voteMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one vote by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<VoteDTO> findOne(Long id) {
        log.debug("Request to get Vote : {}", id);
        return voteRepository.findById(id)
            .map(voteMapper::toDto);
    }

    /**
     * Delete the vote by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Vote : {}", id);
        voteRepository.deleteById(id);
    }

    @Override
    public Boolean didCurrentUserAlreadyVote(Long pollId) {
        log.debug("Request to check if current user Already voted : {}",pollId);
        return voteRepository.didCurrentUserAlreadyVote(pollId);
    }

    @Override
    public Long getVotedOption(Long pollId) {
        log.debug("Request to get current user voted option : {}",pollId);
        return voteRepository.getVotedOption(pollId);
    }

    @Override
    public Long getOptionPercentage(Long optionId) {
        log.debug("Request to get option percentage : {}",optionId);
        Long totalPollVotes = voteRepository.getPollTotalVotesByOptionID(optionId);
        if(totalPollVotes==null || totalPollVotes==0) return (long)0;
        Long totalOptionVotes = voteRepository.getOptionTotalVotes(optionId);
        if(totalOptionVotes==null || totalOptionVotes==0) return (long)0;
        double t = totalOptionVotes.doubleValue() / totalPollVotes.doubleValue();
        return Math.round(t*100);
    }

    @Override
    public Long getPollTotalVotes(Long pollId) {
        log.debug("Request to get poll total votes : {}", pollId);
        Long result = voteRepository.getPollTotalVotes(pollId);
        return (result!=null ? result : 0);
    }

    @Override
    public Long getTotalVotesCount() {
        log.debug("Request to get total votes count");
        Long result = voteRepository.CountAllVotes();
        return (result!=null ? result : 0);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserDTO> getUsersThatVoted(Long pollId) {
        log.debug("Request to get Users that voted on poll : {}", pollId);
        return voteRepository.getUsersThatVotedOnPoll(pollId)
                                .stream()
                                .map(UserDTO::new)
                                .collect(Collectors.toCollection(LinkedList::new));
    }
}
