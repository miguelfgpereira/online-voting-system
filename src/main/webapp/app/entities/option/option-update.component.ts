import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IOption, Option } from 'app/shared/model/option.model';
import { OptionService } from './option.service';
import { IPoll } from 'app/shared/model/poll.model';
import { PollService } from 'app/entities/poll/poll.service';

@Component({
  selector: 'jhi-option-update',
  templateUrl: './option-update.component.html'
})
export class OptionUpdateComponent implements OnInit {
  isSaving: boolean;

  polls: IPoll[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    pollId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected optionService: OptionService,
    protected pollService: PollService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ option }) => {
      this.updateForm(option);
    });
    this.pollService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IPoll[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPoll[]>) => response.body)
      )
      .subscribe((res: IPoll[]) => (this.polls = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(option: IOption) {
    this.editForm.patchValue({
      id: option.id,
      name: option.name,
      pollId: option.pollId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const option = this.createFromForm();
    if (option.id !== undefined) {
      this.subscribeToSaveResponse(this.optionService.update(option));
    } else {
      this.subscribeToSaveResponse(this.optionService.create(option));
    }
  }

  private createFromForm(): IOption {
    return {
      ...new Option(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      pollId: this.editForm.get(['pollId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOption>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackPollById(index: number, item: IPoll) {
    return item.id;
  }
}
