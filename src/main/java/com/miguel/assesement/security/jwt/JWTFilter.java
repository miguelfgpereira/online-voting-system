package com.miguel.assesement.security.jwt;

import com.miguel.assesement.web.rest.AccountResource;
import io.github.jhipster.config.JHipsterDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Filters incoming requests and installs a Spring Security principal if a header corresponding to a valid user is
 * found.
 */
public class JWTFilter extends GenericFilterBean {

    private final Logger log = LoggerFactory.getLogger(JWTFilter.class);
    public static final String AUTHORIZATION_HEADER = "Authorization";

    private TokenProvider tokenProvider;

    public JWTFilter(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
        throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String jwt = resolveToken(httpServletRequest);
        if (StringUtils.hasText(jwt) && this.tokenProvider.validateToken(jwt)) {
            Authentication authentication = this.tokenProvider.getAuthentication(jwt);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private String resolveToken(HttpServletRequest request){
        String bearerToken = request.getHeader(AUTHORIZATION_HEADER);

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
//        bearerToken = getFromSession(request);
        if(bearerToken == null || bearerToken.trim().isEmpty())
            bearerToken = getFromCookie(request);
//        log.debug("Bearer Token From Session : {}",bearerToken);
        return bearerToken;
//        if(bearerToken!=null) {
//            return bearerToken;
//        }
//        return null;
    }

    private String getFromSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(session==null) {
            //log.debug("Bearer Token Session is null");
            return "";
        }
        if(!request.isRequestedSessionIdValid())
            session = request.getSession(true);
        String jwt = (String) session.getAttribute("jwt");
        //if(jwt==null) log.debug("JWT Token is null");
        return (jwt==null ? "" : jwt);
    }

    private String getFromCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if(cookies!=null)
            for(Cookie c : cookies)
                if(c.getName().equals("jwt"))
                    return c.getValue();

        return "";
    }
}
