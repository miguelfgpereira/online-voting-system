package com.miguel.assesement.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Poll.
 */
@Entity
@Table(name = "poll")
public class Poll implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Size(min = 5)
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "initial_date", nullable = false)
    private LocalDate initialDate;

    @NotNull
    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    @OneToMany(mappedBy = "poll")
    private Set<Option> options = new HashSet<>();

    @OneToMany(mappedBy = "poll")
    private Set<Vote> votes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("polls")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("polls")
    private Category category;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Poll name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Poll description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getInitialDate() {
        return initialDate;
    }

    public Poll initialDate(LocalDate initialDate) {
        this.initialDate = initialDate;
        return this;
    }

    public void setInitialDate(LocalDate initialDate) {
        this.initialDate = initialDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Poll endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Set<Option> getOptions() {
        return options;
    }

    public Poll options(Set<Option> options) {
        this.options = options;
        return this;
    }

    public Poll addOptions(Option option) {
        this.options.add(option);
        option.setPoll(this);
        return this;
    }

    public Poll removeOptions(Option option) {
        this.options.remove(option);
        option.setPoll(null);
        return this;
    }

    public void setOptions(Set<Option> options) {
        this.options = options;
    }

    public Set<Vote> getVotes() {
        return votes;
    }

    public Poll votes(Set<Vote> votes) {
        this.votes = votes;
        return this;
    }

    public Poll addVotes(Vote vote) {
        this.votes.add(vote);
        vote.setPoll(this);
        return this;
    }

    public Poll removeVotes(Vote vote) {
        this.votes.remove(vote);
        vote.setPoll(null);
        return this;
    }

    public void setVotes(Set<Vote> votes) {
        this.votes = votes;
    }

    public User getUser() {
        return user;
    }

    public Poll user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public Poll category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Poll)) {
            return false;
        }
        return id != null && id.equals(((Poll) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Poll{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", initialDate='" + getInitialDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            "}";
    }
}
