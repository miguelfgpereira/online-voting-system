import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OvsSharedModule } from 'app/shared/shared.module';
import { VoteComponent } from './vote.component';
import { VoteDetailComponent } from './vote-detail.component';
import { VoteUpdateComponent } from './vote-update.component';
import { VoteDeletePopupComponent, VoteDeleteDialogComponent } from './vote-delete-dialog.component';
import { voteRoute, votePopupRoute } from './vote.route';

const ENTITY_STATES = [...voteRoute, ...votePopupRoute];

@NgModule({
  imports: [OvsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [VoteComponent, VoteDetailComponent, VoteUpdateComponent, VoteDeleteDialogComponent, VoteDeletePopupComponent],
  entryComponents: [VoteDeleteDialogComponent]
})
export class OvsVoteModule {}
