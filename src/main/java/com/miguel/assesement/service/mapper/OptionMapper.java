package com.miguel.assesement.service.mapper;

import com.miguel.assesement.domain.*;
import com.miguel.assesement.service.dto.OptionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Option} and its DTO {@link OptionDTO}.
 */
@Mapper(componentModel = "spring", uses = {PollMapper.class})
public interface OptionMapper extends EntityMapper<OptionDTO, Option> {

    @Mapping(source = "poll.id", target = "pollId")
    @Mapping(source = "poll.name", target = "pollName")
    OptionDTO toDto(Option option);

    @Mapping(target = "votes", ignore = true)
    @Mapping(target = "removeVotes", ignore = true)
    @Mapping(source = "pollId", target = "poll")
    Option toEntity(OptionDTO optionDTO);

    default Option fromId(Long id) {
        if (id == null) {
            return null;
        }
        Option option = new Option();
        option.setId(id);
        return option;
    }
}
