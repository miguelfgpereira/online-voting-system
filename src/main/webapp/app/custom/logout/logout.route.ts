import { Routes } from '@angular/router';

import { LogoutComponent } from './logout.component';

export const logoutRoute: Routes = [
  {
    path: 'bye',
    component: LogoutComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Logout'
    }
  }
];
