import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'poll',
        loadChildren: () => import('./poll/poll.module').then(m => m.OvsPollModule)
      },
      {
        path: 'option',
        loadChildren: () => import('./option/option.module').then(m => m.OvsOptionModule)
      },
      {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.OvsCategoryModule)
      },
      {
        path: 'vote',
        loadChildren: () => import('./vote/vote.module').then(m => m.OvsVoteModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class OvsEntityModule {}
