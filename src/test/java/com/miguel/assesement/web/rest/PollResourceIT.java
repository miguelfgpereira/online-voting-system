package com.miguel.assesement.web.rest;

import com.miguel.assesement.OvsApp;
import com.miguel.assesement.domain.Poll;
import com.miguel.assesement.domain.Option;
import com.miguel.assesement.domain.Vote;
import com.miguel.assesement.domain.User;
import com.miguel.assesement.domain.Category;
import com.miguel.assesement.repository.PollRepository;
import com.miguel.assesement.service.PollService;
import com.miguel.assesement.service.dto.PollDTO;
import com.miguel.assesement.service.mapper.PollMapper;
import com.miguel.assesement.web.rest.errors.ExceptionTranslator;
import com.miguel.assesement.service.dto.PollCriteria;
import com.miguel.assesement.service.PollQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.miguel.assesement.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PollResource} REST controller.
 */
@SpringBootTest(classes = OvsApp.class)
public class PollResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_INITIAL_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_INITIAL_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_INITIAL_DATE = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_END_DATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private PollRepository pollRepository;

    @Autowired
    private PollMapper pollMapper;

    @Autowired
    private PollService pollService;

    @Autowired
    private PollQueryService pollQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPollMockMvc;

    private Poll poll;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PollResource pollResource = new PollResource(pollService, pollQueryService);
        this.restPollMockMvc = MockMvcBuilders.standaloneSetup(pollResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Poll createEntity(EntityManager em) {
        Poll poll = new Poll()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .initialDate(DEFAULT_INITIAL_DATE)
            .endDate(DEFAULT_END_DATE);
        return poll;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Poll createUpdatedEntity(EntityManager em) {
        Poll poll = new Poll()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .initialDate(UPDATED_INITIAL_DATE)
            .endDate(UPDATED_END_DATE);
        return poll;
    }

    @BeforeEach
    public void initTest() {
        poll = createEntity(em);
    }

    @Test
    @Transactional
    public void createPoll() throws Exception {
        int databaseSizeBeforeCreate = pollRepository.findAll().size();

        // Create the Poll
        PollDTO pollDTO = pollMapper.toDto(poll);
        restPollMockMvc.perform(post("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isCreated());

        // Validate the Poll in the database
        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeCreate + 1);
        Poll testPoll = pollList.get(pollList.size() - 1);
        assertThat(testPoll.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPoll.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPoll.getInitialDate()).isEqualTo(DEFAULT_INITIAL_DATE);
        assertThat(testPoll.getEndDate()).isEqualTo(DEFAULT_END_DATE);
    }

    @Test
    @Transactional
    public void createPollWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pollRepository.findAll().size();

        // Create the Poll with an existing ID
        poll.setId(1L);
        PollDTO pollDTO = pollMapper.toDto(poll);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPollMockMvc.perform(post("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Poll in the database
        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = pollRepository.findAll().size();
        // set the field null
        poll.setName(null);

        // Create the Poll, which fails.
        PollDTO pollDTO = pollMapper.toDto(poll);

        restPollMockMvc.perform(post("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isBadRequest());

        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInitialDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = pollRepository.findAll().size();
        // set the field null
        poll.setInitialDate(null);

        // Create the Poll, which fails.
        PollDTO pollDTO = pollMapper.toDto(poll);

        restPollMockMvc.perform(post("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isBadRequest());

        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEndDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = pollRepository.findAll().size();
        // set the field null
        poll.setEndDate(null);

        // Create the Poll, which fails.
        PollDTO pollDTO = pollMapper.toDto(poll);

        restPollMockMvc.perform(post("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isBadRequest());

        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPolls() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList
        restPollMockMvc.perform(get("/api/polls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(poll.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].initialDate").value(hasItem(DEFAULT_INITIAL_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getPoll() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get the poll
        restPollMockMvc.perform(get("/api/polls/{id}", poll.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(poll.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.initialDate").value(DEFAULT_INITIAL_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllPollsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where name equals to DEFAULT_NAME
        defaultPollShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the pollList where name equals to UPDATED_NAME
        defaultPollShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPollsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where name not equals to DEFAULT_NAME
        defaultPollShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the pollList where name not equals to UPDATED_NAME
        defaultPollShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPollsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where name in DEFAULT_NAME or UPDATED_NAME
        defaultPollShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the pollList where name equals to UPDATED_NAME
        defaultPollShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPollsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where name is not null
        defaultPollShouldBeFound("name.specified=true");

        // Get all the pollList where name is null
        defaultPollShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllPollsByNameContainsSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where name contains DEFAULT_NAME
        defaultPollShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the pollList where name contains UPDATED_NAME
        defaultPollShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPollsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where name does not contain DEFAULT_NAME
        defaultPollShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the pollList where name does not contain UPDATED_NAME
        defaultPollShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllPollsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where description equals to DEFAULT_DESCRIPTION
        defaultPollShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the pollList where description equals to UPDATED_DESCRIPTION
        defaultPollShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPollsByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where description not equals to DEFAULT_DESCRIPTION
        defaultPollShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the pollList where description not equals to UPDATED_DESCRIPTION
        defaultPollShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPollsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultPollShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the pollList where description equals to UPDATED_DESCRIPTION
        defaultPollShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPollsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where description is not null
        defaultPollShouldBeFound("description.specified=true");

        // Get all the pollList where description is null
        defaultPollShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllPollsByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where description contains DEFAULT_DESCRIPTION
        defaultPollShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the pollList where description contains UPDATED_DESCRIPTION
        defaultPollShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPollsByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where description does not contain DEFAULT_DESCRIPTION
        defaultPollShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the pollList where description does not contain UPDATED_DESCRIPTION
        defaultPollShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllPollsByInitialDateIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where initialDate equals to DEFAULT_INITIAL_DATE
        defaultPollShouldBeFound("initialDate.equals=" + DEFAULT_INITIAL_DATE);

        // Get all the pollList where initialDate equals to UPDATED_INITIAL_DATE
        defaultPollShouldNotBeFound("initialDate.equals=" + UPDATED_INITIAL_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByInitialDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where initialDate not equals to DEFAULT_INITIAL_DATE
        defaultPollShouldNotBeFound("initialDate.notEquals=" + DEFAULT_INITIAL_DATE);

        // Get all the pollList where initialDate not equals to UPDATED_INITIAL_DATE
        defaultPollShouldBeFound("initialDate.notEquals=" + UPDATED_INITIAL_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByInitialDateIsInShouldWork() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where initialDate in DEFAULT_INITIAL_DATE or UPDATED_INITIAL_DATE
        defaultPollShouldBeFound("initialDate.in=" + DEFAULT_INITIAL_DATE + "," + UPDATED_INITIAL_DATE);

        // Get all the pollList where initialDate equals to UPDATED_INITIAL_DATE
        defaultPollShouldNotBeFound("initialDate.in=" + UPDATED_INITIAL_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByInitialDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where initialDate is not null
        defaultPollShouldBeFound("initialDate.specified=true");

        // Get all the pollList where initialDate is null
        defaultPollShouldNotBeFound("initialDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPollsByInitialDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where initialDate is greater than or equal to DEFAULT_INITIAL_DATE
        defaultPollShouldBeFound("initialDate.greaterThanOrEqual=" + DEFAULT_INITIAL_DATE);

        // Get all the pollList where initialDate is greater than or equal to UPDATED_INITIAL_DATE
        defaultPollShouldNotBeFound("initialDate.greaterThanOrEqual=" + UPDATED_INITIAL_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByInitialDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where initialDate is less than or equal to DEFAULT_INITIAL_DATE
        defaultPollShouldBeFound("initialDate.lessThanOrEqual=" + DEFAULT_INITIAL_DATE);

        // Get all the pollList where initialDate is less than or equal to SMALLER_INITIAL_DATE
        defaultPollShouldNotBeFound("initialDate.lessThanOrEqual=" + SMALLER_INITIAL_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByInitialDateIsLessThanSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where initialDate is less than DEFAULT_INITIAL_DATE
        defaultPollShouldNotBeFound("initialDate.lessThan=" + DEFAULT_INITIAL_DATE);

        // Get all the pollList where initialDate is less than UPDATED_INITIAL_DATE
        defaultPollShouldBeFound("initialDate.lessThan=" + UPDATED_INITIAL_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByInitialDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where initialDate is greater than DEFAULT_INITIAL_DATE
        defaultPollShouldNotBeFound("initialDate.greaterThan=" + DEFAULT_INITIAL_DATE);

        // Get all the pollList where initialDate is greater than SMALLER_INITIAL_DATE
        defaultPollShouldBeFound("initialDate.greaterThan=" + SMALLER_INITIAL_DATE);
    }


    @Test
    @Transactional
    public void getAllPollsByEndDateIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where endDate equals to DEFAULT_END_DATE
        defaultPollShouldBeFound("endDate.equals=" + DEFAULT_END_DATE);

        // Get all the pollList where endDate equals to UPDATED_END_DATE
        defaultPollShouldNotBeFound("endDate.equals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByEndDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where endDate not equals to DEFAULT_END_DATE
        defaultPollShouldNotBeFound("endDate.notEquals=" + DEFAULT_END_DATE);

        // Get all the pollList where endDate not equals to UPDATED_END_DATE
        defaultPollShouldBeFound("endDate.notEquals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByEndDateIsInShouldWork() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where endDate in DEFAULT_END_DATE or UPDATED_END_DATE
        defaultPollShouldBeFound("endDate.in=" + DEFAULT_END_DATE + "," + UPDATED_END_DATE);

        // Get all the pollList where endDate equals to UPDATED_END_DATE
        defaultPollShouldNotBeFound("endDate.in=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByEndDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where endDate is not null
        defaultPollShouldBeFound("endDate.specified=true");

        // Get all the pollList where endDate is null
        defaultPollShouldNotBeFound("endDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPollsByEndDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where endDate is greater than or equal to DEFAULT_END_DATE
        defaultPollShouldBeFound("endDate.greaterThanOrEqual=" + DEFAULT_END_DATE);

        // Get all the pollList where endDate is greater than or equal to UPDATED_END_DATE
        defaultPollShouldNotBeFound("endDate.greaterThanOrEqual=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByEndDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where endDate is less than or equal to DEFAULT_END_DATE
        defaultPollShouldBeFound("endDate.lessThanOrEqual=" + DEFAULT_END_DATE);

        // Get all the pollList where endDate is less than or equal to SMALLER_END_DATE
        defaultPollShouldNotBeFound("endDate.lessThanOrEqual=" + SMALLER_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByEndDateIsLessThanSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where endDate is less than DEFAULT_END_DATE
        defaultPollShouldNotBeFound("endDate.lessThan=" + DEFAULT_END_DATE);

        // Get all the pollList where endDate is less than UPDATED_END_DATE
        defaultPollShouldBeFound("endDate.lessThan=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllPollsByEndDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        // Get all the pollList where endDate is greater than DEFAULT_END_DATE
        defaultPollShouldNotBeFound("endDate.greaterThan=" + DEFAULT_END_DATE);

        // Get all the pollList where endDate is greater than SMALLER_END_DATE
        defaultPollShouldBeFound("endDate.greaterThan=" + SMALLER_END_DATE);
    }


    @Test
    @Transactional
    public void getAllPollsByOptionsIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);
        Option options = OptionResourceIT.createEntity(em);
        em.persist(options);
        em.flush();
        poll.addOptions(options);
        pollRepository.saveAndFlush(poll);
        Long optionsId = options.getId();

        // Get all the pollList where options equals to optionsId
        defaultPollShouldBeFound("optionsId.equals=" + optionsId);

        // Get all the pollList where options equals to optionsId + 1
        defaultPollShouldNotBeFound("optionsId.equals=" + (optionsId + 1));
    }


    @Test
    @Transactional
    public void getAllPollsByVotesIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);
        Vote votes = VoteResourceIT.createEntity(em);
        em.persist(votes);
        em.flush();
        poll.addVotes(votes);
        pollRepository.saveAndFlush(poll);
        Long votesId = votes.getId();

        // Get all the pollList where votes equals to votesId
        defaultPollShouldBeFound("votesId.equals=" + votesId);

        // Get all the pollList where votes equals to votesId + 1
        defaultPollShouldNotBeFound("votesId.equals=" + (votesId + 1));
    }


    @Test
    @Transactional
    public void getAllPollsByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        poll.setUser(user);
        pollRepository.saveAndFlush(poll);
        Long userId = user.getId();

        // Get all the pollList where user equals to userId
        defaultPollShouldBeFound("userId.equals=" + userId);

        // Get all the pollList where user equals to userId + 1
        defaultPollShouldNotBeFound("userId.equals=" + (userId + 1));
    }


    @Test
    @Transactional
    public void getAllPollsByCategoryIsEqualToSomething() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);
        Category category = CategoryResourceIT.createEntity(em);
        em.persist(category);
        em.flush();
        poll.setCategory(category);
        pollRepository.saveAndFlush(poll);
        Long categoryId = category.getId();

        // Get all the pollList where category equals to categoryId
        defaultPollShouldBeFound("categoryId.equals=" + categoryId);

        // Get all the pollList where category equals to categoryId + 1
        defaultPollShouldNotBeFound("categoryId.equals=" + (categoryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPollShouldBeFound(String filter) throws Exception {
        restPollMockMvc.perform(get("/api/polls?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(poll.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].initialDate").value(hasItem(DEFAULT_INITIAL_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));

        // Check, that the count call also returns 1
        restPollMockMvc.perform(get("/api/polls/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPollShouldNotBeFound(String filter) throws Exception {
        restPollMockMvc.perform(get("/api/polls?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPollMockMvc.perform(get("/api/polls/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPoll() throws Exception {
        // Get the poll
        restPollMockMvc.perform(get("/api/polls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePoll() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        int databaseSizeBeforeUpdate = pollRepository.findAll().size();

        // Update the poll
        Poll updatedPoll = pollRepository.findById(poll.getId()).get();
        // Disconnect from session so that the updates on updatedPoll are not directly saved in db
        em.detach(updatedPoll);
        updatedPoll
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .initialDate(UPDATED_INITIAL_DATE)
            .endDate(UPDATED_END_DATE);
        PollDTO pollDTO = pollMapper.toDto(updatedPoll);

        restPollMockMvc.perform(put("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isOk());

        // Validate the Poll in the database
        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeUpdate);
        Poll testPoll = pollList.get(pollList.size() - 1);
        assertThat(testPoll.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPoll.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPoll.getInitialDate()).isEqualTo(UPDATED_INITIAL_DATE);
        assertThat(testPoll.getEndDate()).isEqualTo(UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPoll() throws Exception {
        int databaseSizeBeforeUpdate = pollRepository.findAll().size();

        // Create the Poll
        PollDTO pollDTO = pollMapper.toDto(poll);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPollMockMvc.perform(put("/api/polls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pollDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Poll in the database
        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePoll() throws Exception {
        // Initialize the database
        pollRepository.saveAndFlush(poll);

        int databaseSizeBeforeDelete = pollRepository.findAll().size();

        // Delete the poll
        restPollMockMvc.perform(delete("/api/polls/{id}", poll.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Poll> pollList = pollRepository.findAll();
        assertThat(pollList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Poll.class);
        Poll poll1 = new Poll();
        poll1.setId(1L);
        Poll poll2 = new Poll();
        poll2.setId(poll1.getId());
        assertThat(poll1).isEqualTo(poll2);
        poll2.setId(2L);
        assertThat(poll1).isNotEqualTo(poll2);
        poll1.setId(null);
        assertThat(poll1).isNotEqualTo(poll2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PollDTO.class);
        PollDTO pollDTO1 = new PollDTO();
        pollDTO1.setId(1L);
        PollDTO pollDTO2 = new PollDTO();
        assertThat(pollDTO1).isNotEqualTo(pollDTO2);
        pollDTO2.setId(pollDTO1.getId());
        assertThat(pollDTO1).isEqualTo(pollDTO2);
        pollDTO2.setId(2L);
        assertThat(pollDTO1).isNotEqualTo(pollDTO2);
        pollDTO1.setId(null);
        assertThat(pollDTO1).isNotEqualTo(pollDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pollMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pollMapper.fromId(null)).isNull();
    }
}
