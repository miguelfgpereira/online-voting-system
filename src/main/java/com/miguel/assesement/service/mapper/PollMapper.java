package com.miguel.assesement.service.mapper;

import com.miguel.assesement.domain.*;
import com.miguel.assesement.service.dto.PollDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Poll} and its DTO {@link PollDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, CategoryMapper.class})
public interface PollMapper extends EntityMapper<PollDTO, Poll> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "category.name", target = "categoryName")
    PollDTO toDto(Poll poll);

    @Mapping(target = "options", ignore = true)
    @Mapping(target = "removeOptions", ignore = true)
    @Mapping(target = "votes", ignore = true)
    @Mapping(target = "removeVotes", ignore = true)
    @Mapping(source = "userId", target = "user")
    @Mapping(source = "categoryId", target = "category")
    Poll toEntity(PollDTO pollDTO);

    default Poll fromId(Long id) {
        if (id == null) {
            return null;
        }
        Poll poll = new Poll();
        poll.setId(id);
        return poll;
    }
}
