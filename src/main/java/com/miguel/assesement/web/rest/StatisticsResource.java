package com.miguel.assesement.web.rest;

import com.miguel.assesement.service.StatisticsService;
import com.miguel.assesement.service.dto.CategoryDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class StatisticsResource {

    private final Logger log = LoggerFactory.getLogger(StatisticsResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StatisticsService statisticsService;

    public StatisticsResource(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @GetMapping("/applicationStatistics")
    public ResponseEntity<Map<String,String>> applicationStatistics() {
        log.debug("REST request for Application Statistics");
        return ResponseEntity.ok().body(statisticsService.getApplicationStatistics());
    }

    @GetMapping("/applicationStatistics/getPopularCategories/{number}")
    public ResponseEntity<List<CategoryDTO>> getPopularCategories(@PathVariable int number) {
        log.debug("REST request to get top {} popular categories",number);
        return ResponseEntity.ok().body(statisticsService.getTopNPopularCategories(number));
    }
}
