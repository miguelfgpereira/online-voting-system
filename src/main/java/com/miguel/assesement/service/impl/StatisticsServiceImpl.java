package com.miguel.assesement.service.impl;

import com.miguel.assesement.domain.Poll;
import com.miguel.assesement.service.*;
import com.miguel.assesement.service.dto.CategoryDTO;
import com.miguel.assesement.service.dto.PollCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class StatisticsServiceImpl implements StatisticsService {

    private final Logger log = LoggerFactory.getLogger(StatisticsServiceImpl.class);

    private PollService pollService;
    private VoteService voteService;
    private CategoryService categoryService;
    private OptionService optionService;
    public UserService userService;
    public AuditEventService auditEventService;

    public StatisticsServiceImpl(AuditEventService auditEventService, UserService userService, PollService pollService, VoteService voteService, CategoryService categoryService, OptionService optionService) {
        this.pollService = pollService;
        this.voteService = voteService;
        this.categoryService = categoryService;
        this.optionService = optionService;
        this.userService = userService;
        this.auditEventService = auditEventService;
    }


    @Override
    public Map<String, String> getApplicationStatistics() {
        log.debug("Request to get Application statistics");
        Map<String, String> result = new HashMap<String, String>();
        result.put("options", Long.toString(optionService.getTotalOptionsCount()));
        result.put("polls", Long.toString(pollService.getTotalPollsCount()));
        result.put("votes", Long.toString(voteService.getTotalVotesCount()));
        result.put("categories", Long.toString(categoryService.getTotalCategoriesCount()));
        result.put("users", Long.toString(userService.getTotalUserCount()));
//        result.put("popularCategoriesPercentage", categoryService.getPopularCategoriesPercentage());
//        Daily Logins
        result.put("dailyLogins", Long.toString(auditEventService.CountSuccessfullLoginsToday()));
//        Unique Daily Logins
        result.put("uniqueDailyLogins", Long.toString(auditEventService.CountUniqueSuccessfullLoginsToday()));
//        User Registered this month (with increase bar)
//        result.put("userRegisteredThisMonth", Long.toString(userService.getUserRegisteredThisMonthCount()));
//        Polls created this month (with increase bar) - rever
//        Polls created this week (with increase bar) - rever
        return result;
    }

    @Override
    public List<CategoryDTO> getTopNPopularCategories(int number) {
        log.debug("Request to get top {} popular categories", number);
        return categoryService.getPopularCategories(number);
    }


}
