import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Poll } from 'app/shared/model/poll.model';
import { PollService } from './poll.service';
import { PollComponent } from './poll.component';
import { PollDetailComponent } from './poll-detail.component';
import { PollUpdateComponent } from './poll-update.component';
import { PollDeletePopupComponent } from './poll-delete-dialog.component';
import { IPoll } from 'app/shared/model/poll.model';

@Injectable({ providedIn: 'root' })
export class PollResolve implements Resolve<IPoll> {
  constructor(private service: PollService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPoll> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Poll>) => response.ok),
        map((poll: HttpResponse<Poll>) => poll.body)
      );
    }
    return of(new Poll());
  }
}

export const pollRoute: Routes = [
  {
    path: '',
    component: PollComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'Polls'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PollDetailComponent,
    resolve: {
      poll: PollResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Polls'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PollUpdateComponent,
    resolve: {
      poll: PollResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Polls'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PollUpdateComponent,
    resolve: {
      poll: PollResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Polls'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const pollPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PollDeletePopupComponent,
    resolve: {
      poll: PollResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Polls'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
