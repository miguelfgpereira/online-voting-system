package com.miguel.assesement.web.mvc;

import com.miguel.assesement.security.AuthoritiesConstants;
import com.miguel.assesement.security.SecurityUtils;
import com.miguel.assesement.security.jwt.TokenProvider;
import com.miguel.assesement.service.UserService;
import com.miguel.assesement.service.UsernameAlreadyUsedException;
import com.miguel.assesement.service.dto.UserDTO;
import com.miguel.assesement.web.rest.UserJWTController;
import com.miguel.assesement.web.rest.errors.EmailAlreadyUsedException;
import com.miguel.assesement.web.rest.errors.LoginAlreadyUsedException;
import com.miguel.assesement.web.rest.vm.LoginVM;
import com.miguel.assesement.web.rest.vm.ManagedUserVM;
import io.github.jhipster.config.JHipsterConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping("mvc")
public class LoginController {
    private final Logger log = LoggerFactory.getLogger(LoginController.class);

    private final TokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private MvcUtils mvcUtils;
    private UserService userService;

    public LoginController(UserService userService, MvcUtils mvcUtils,TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.mvcUtils = mvcUtils;
        this.userService = userService;

    }

    @GetMapping("login")
    public ModelAndView login() {
        log.debug("MVC Request to show Login Page");
        ModelAndView result = mvcUtils.getModelAndView(null);
        result.setViewName("login");
        return result;
    }

    @PostMapping("login")
    public ModelAndView login(LoginVM loginVM, HttpSession session, RedirectAttributes redirectAttributes, HttpServletResponse response) {
        log.debug("MVC Request to Login : {}", loginVM.getUsername());
        try {
            UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

            Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = tokenProvider.createToken(authentication, false);

//            session.setAttribute("jwt", jwt);
            addCookie(response,jwt);
            redirectAttributes.addFlashAttribute("success", "You have successfully logged in!");
            return new ModelAndView("loginLoading");
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute("error", "You have failed to logged in!");
            return new ModelAndView("redirect:/mvc/login");
        }
    }

    private void addCookie(HttpServletResponse response, String jwt) {
        Cookie cookie = new Cookie("jwt",jwt);
        cookie.setMaxAge(-1);
        response.addCookie(cookie);
    }

    @GetMapping("logout")
    public RedirectView logout(HttpSession session, RedirectAttributes redirectAttributes, HttpServletResponse response) {
        log.debug("MVC Request to logout : {}", SecurityUtils.getCurrentUserLogin());
        removeCookie(response);
        redirectAttributes.addFlashAttribute("success", "You have successfully logged out!");
        return new RedirectView("/mvc");
    }

    private void removeCookie(HttpServletResponse response) {
        Cookie cookie = new Cookie("jwt","");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

    @GetMapping("register")
    public ModelAndView register() {
        log.debug("MVC Request to show Register Page");
        ModelAndView result = mvcUtils.getModelAndView(null);
        result.setViewName("register");
        return result;
    }

    @PostMapping("register")
    public RedirectView register(ManagedUserVM managedUserVM, HttpSession session, RedirectAttributes redirectAttributes) {
        log.debug("MVC Request to Register : {}", managedUserVM.getLogin());
        try {
            managedUserVM.setImageUrl(null);
//            Set<String> authorities = new HashSet<>();
//            authorities.add(AuthoritiesConstants.USER);
//            managedUserVM.setAuthorities(authorities);
            managedUserVM.setCreatedBy(null);
            managedUserVM.setCreatedDate(Instant.now());
            managedUserVM.setActivated(true);
            managedUserVM.setLastModifiedBy(null);
            managedUserVM.setLastModifiedDate(Instant.now());
            userService.registerUserMVC(managedUserVM, managedUserVM.getPassword(), true);
            redirectAttributes.addFlashAttribute("success","Your account has been sucessfully created! You may now login.");
            return new RedirectView("/mvc/login");
        } catch (Exception ex) {
            if(ex instanceof EmailAlreadyUsedException || ex instanceof com.miguel.assesement.service.EmailAlreadyUsedException)
                redirectAttributes.addFlashAttribute("error", "That email has already been used!");
            else if(ex instanceof LoginAlreadyUsedException || ex instanceof UsernameAlreadyUsedException)
                redirectAttributes.addFlashAttribute("error", "That username has already been used!");
            else
                redirectAttributes.addFlashAttribute("error", "There has been an error creating your account!");
            return new RedirectView("/mvc/register");
        }
    }
}
