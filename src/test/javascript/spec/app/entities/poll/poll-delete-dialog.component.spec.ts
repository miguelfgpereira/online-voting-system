import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { OvsTestModule } from '../../../test.module';
import { PollDeleteDialogComponent } from 'app/entities/poll/poll-delete-dialog.component';
import { PollService } from 'app/entities/poll/poll.service';

describe('Component Tests', () => {
  describe('Poll Management Delete Component', () => {
    let comp: PollDeleteDialogComponent;
    let fixture: ComponentFixture<PollDeleteDialogComponent>;
    let service: PollService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [OvsTestModule],
        declarations: [PollDeleteDialogComponent]
      })
        .overrideTemplate(PollDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PollDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PollService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
