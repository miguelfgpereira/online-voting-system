import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OvsSharedModule } from 'app/shared/shared.module';
import { PollComponent } from './poll.component';
import { PollDetailComponent } from './poll-detail.component';
import { PollUpdateComponent } from './poll-update.component';
import { PollDeletePopupComponent, PollDeleteDialogComponent } from './poll-delete-dialog.component';
import { pollRoute, pollPopupRoute } from './poll.route';

const ENTITY_STATES = [...pollRoute, ...pollPopupRoute];

@NgModule({
  imports: [OvsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [PollComponent, PollDetailComponent, PollUpdateComponent, PollDeleteDialogComponent, PollDeletePopupComponent],
  entryComponents: [PollDeleteDialogComponent]
})
export class OvsPollModule {}
